<?php

namespace Fuel\Tasks;

use Fuel\Core\Cli;
use Fuel\Core\Config;
use LogLib\LogLib;

/**
 * Description of news_site_rss
 * RUN BATCH: 
 * php oil refine NewsSitesRss
 * FUEL_ENV=test php oil refine NewsSitesRss
 * FUEL_ENV=production php oil refine NewsSitesRss
 * @author tuancd
 */
class NewsSitesRss {

    protected static $_channel_date = 'channel_date', $_item_date = 'item_date';
    protected static $date_format_array = array(
        0 => array(
            'channel_date' => 'dc:date',
            'item_date' => 'dc:date'
        ),
        1 => array(
            'channel_date' => 'lastBuildDate',
            'item_date' => 'pubDate'
        ),
        2 => array(
            'channel_date' => 'pubDate',
            'item_date' => 'pubDate'
        )
    );

    public static function run() {
        try {
            LogLib::info("Start batch NewsSitesRss", __METHOD__);
            Cli::write("Begin Processing . . . . ! \n");
            include(APPPATH . '/classes/lib/simple_html_dom.php');

            $t = time() - Config::get('time_batch', true);
            $param = array('time' => $t);

            // Get list site and link rss
            $lsturl = \Model_News_Sites_Rss::get_list($param);
            $countFeed = 0;
            foreach ($lsturl[1] as $url) {
                try {
                    $count = 0;
                    Cli::write("----------- BEGIN  " . $url['url'] . " ----------- \n \n");
                    // Handler link if link die
                    $str = null;
                    $str = @file_get_contents($url['url']);
                    if ($str === FALSE) {
                        Cli::write("link: " . $url['url'] . " UNAVAILABLE!!  \n");
                    } else {
                        $html=null;
                        $html = str_get_html($str);
                        // Get textDate of channel first and get TagDate detect
                        list($textDate, $tagDate) = self::getTagDate($str);
                        //get distribution_date 
                        $distribution_date = strtotime($textDate);
                        $title_tag = !empty($url['tag_item_title']) ? $url['tag_item_title'] : "title";
                        $link_tag = !empty($url['tag_item_link']) ? $url['tag_item_link'] : "link";
                        $description_tag = !empty($url['tag_item_description']) ? $url['tag_item_description'] : "description";
                        $date_tag = !empty($url['tag_item_date']) ? $url['tag_item_date'] : $tagDate[self::$_item_date];
                        foreach ($html->find('item') as $article) {
                           
                            try {
                                $title = self::getContentByTag($article->innertext, $title_tag, true);
                                $link = self::getContentByTag($article->innertext, $link_tag);
                                $description = self::getContentByTag($article->innertext, $description_tag, true);
                                $publicDate = self::getContentByTag($article->innertext, $date_tag, true);

                                $news_feeds_param = array(
                                    'news_site_id' => $url['news_site_id'],
                                    'title' => $title,
                                    'short_content' => $description,
                                    'detail_url' => $link,
                                    'distribution_date' => empty($publicDate) ? $distribution_date : strtotime($publicDate),
                                    'id' => $url['id']
                                );
                                $result = \Model_News_Feed::add_update_for_batch($news_feeds_param);
                                if ($result === false || $result === 0) {
                                    Cli::write(" Import fail: " . $link . " \n");
                                } elseif ($result === true) {
                                    Cli::write("\t-link: $link is Exists \n");
                                } else if ($result > 0) {
                                    $count++;
                                    $countFeed ++;
                                    Cli::write("\t-Title: $title \n");
                                    Cli::write("\t-link: $link \n");
                                    Cli::write("\t-Description: $description \n \n");
                                    LogLib::info("Import feed {$link}", __METHOD__, $article);
                                }
                            } catch (Exception $exc) {
                                LogLib::error($exc->getMessage(), __METHOD__, $article);
                                Cli::write("error $count \n\n");
                            }
                        }
                    }
                    Cli::write("----------- END  " . $url['url'] . " ----------- \n \n");
                } catch (Exception $exc) {
                    Cli::write("Has error while processing data from url " . $url['url'] . "\n");
                }
            }
            Cli::write("Processing Done . . . . !. Add new successful $countFeed feeds \n");
        } catch (Exception $exc) {
            Cli::write("Has error while processing.");
        }
    }

    static function getContentByTag($stringText = "", $tag = "", $isPlanText = false) {
        try {
            preg_match("/<$tag>(.*)<\/$tag>/", $stringText, $match);
            if (preg_last_error() == PREG_BACKTRACK_LIMIT_ERROR || sizeof($match) == 0) {
                return "";
            } else {
                $string = "";
                if ($isPlanText) {
                    $string = str_replace("<![CDATA[", "", $match[1]);
                    $string = strip_tags(str_replace("]]>", "", $string));
                } else {
                    $string = $match[1];
                }
                if (($_index = strpos($string, "</$tag>")) !== false) {
                    $string = substr($string, 0, $_index);
                }
                preg_match("/&#[xX][a-fA-F0-9]+/", $string, $match);
                if (preg_last_error() == PREG_BACKTRACK_LIMIT_ERROR || sizeof($match) == 0) {
                    return trim($string);
                } else {
                    return html_entity_decode(trim($string), ENT_QUOTES);
                }
            }
        } catch (Exception $exc) {
            return "";
        }
    }

    static function getTagDate($str) {
        $max = sizeof(self::$date_format_array);
        for ($i = 0; $i < $max; $i ++) {
            $text = self::getContentByTag($str, self::$date_format_array[$i]['channel_date']);
            if (!empty($text)) {
                return list($textDate, $tagDate) = array($text, self::$date_format_array[$i]);
                break;
            }
        }
    }

}
