<?php
return array(
    'driver' => 'file',
    'expiration' => null,
    'path' => APPPATH . 'cache/',  
	'key' => array(		
		'setting_all' => 24*60*60,
	)
);
