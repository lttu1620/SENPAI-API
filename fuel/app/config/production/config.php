<?php
$envConf = array(
	'adm_url' => 'https://sensei.sen-pai.jp/',
	'img_url' => 'https://img.sen-pai.jp/',
	'fe_url' => 'https://sen-pai.jp/',
    'facebook' => array(
        'app_id' => '674403482679977',
        'app_secret' => 'ef05554516bac43dd0aecdd0621b0ac8',
    )
);
if (isset($_SERVER['SERVER_NAME'])) {
    if (file_exists(__DIR__ . DIRECTORY_SEPARATOR . $_SERVER['SERVER_NAME'] . '.php')) {
        include_once (__DIR__ . DIRECTORY_SEPARATOR . $_SERVER['SERVER_NAME'] . '.php');
        $envConf = array_merge($envConf, $domainConf);
    }
}
return $envConf;