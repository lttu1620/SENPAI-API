<?php
/**
 * Model to operate to User's functions.
 *
 * @package Model
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Model_User extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'app_id',
        'user_group_id',
        'enrollment_id',
        'number',
        'password',
        'hash',
        'name',
        'grade',
        'mail',
        'mail_parent',
        'last_login_date',
        'memo',
        'type',
        'image_url',
        'sex_id',
        'created',
        'updated',
        'disable',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'users';

    /**
     * Get user detail for mobile.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array|bool Returns the array or the boolean.
     */
    public static function login($param) {
        // Check user by Email is register but not active
//        $options['where'] = array(
//            array('disable', "=", 0),
//            array('number', "=", $param['number'])
//        );
//        $useractivation = \Model_User_Activation::find('first', $options);
//        if (!empty($useractivation)) {
//            // check expried date 
//            if ($useractivation['expire_date'] < time()) {
//                self::errorOther(static::ERROR_CODE_OTHER_1, 'number', 'User not active and has been expired register.'); // User register has expried date
//                return false;
//            } else {
//                self::errorOther(static::ERROR_CODE_OTHER_2, 'number', 'User is  not active.'); // User is not active
//                return false;
//            }
//        } else {
            // Check user with email
            $param['disable'] = 0;
            // check by email
            $data = self::get_detail($param);
            if (!empty($data)) {
//                // write log for login
//                if (isset($param['os'])) {
//                    if ($param['os'] == \Config::get('os')['ios']) {
//                        $l_device = 1;
//                    } elseif ($param['os'] == \Config::get('os')['android']) {
//                        $l_device = 2;
//                    } else {
//                        $l_device = 3;
//                    }
//                }
//                $param = array(
//                    'user_id' => $data['id'],
//                    'login_device' => $l_device
//                );
              \Model_Login_Log::add(array('user_id' => $data['id']));
                return $data;
            } else {
                self::errorNotExist('user_information', 'information_of_user'); // User with email and password not match
                return false;
            }
//        }
    }
    /**
     * Get user login fron sh
     *
     * @author caolp
     * @param array $param Input data.
     * @return array|bool Returns the array or the boolean.
     */
    public static function login_by_number($param) {
        unset($param['password']);
        $param['disable'] = 0;
        // check by email
        $data = self::get_detail($param);
        if (!empty($data)) {
            \Model_Login_Log::add(array('user_id' => $data['id']));
            return $data;
        } else {
            self::errorNotExist('user_information', 'information_of_user'); // User with email and password not match
            return false;
        }
    }
    
     /**
     * Function to get detail user.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Return information of a user.
     */
    public static function get_detail($param) {
        $query = DB::select(
                        'id','number', 'name', 'grade', 'mail', 'last_login_date',
                         'user_group_id', 'enrollment_id', 'memo',
                         DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url), IF(users.sex_id = 1, '" . \Config::get('no_image_user_male') . "', '".\Config::get('no_image_user_female')."')) AS image_url"),
                         'type', 'sex_id',
                        DB::expr("IF(sex_id = 1, 'Male', 'Female') as 'gender'"),
                        'created', 'updated'
                )
                ->from('users')
                ->limit(1)
                ->offset(0);
        if (isset($param['id'])) {
            $query->where('users.id', $param['id']);
        }
        if (isset($param['disable'])) {
            $query->where(self::$_table_name.'.disable',$param['disable']);
        }
        if (!empty($param['mail'])) {
            $query->where(self::$_table_name.'.mail', $param['mail']);
        }
        if (!empty($param['number'])) {
            $query->where(self::$_table_name.'.number', '=', $param['number']);
        }
        if (!empty($param['password']) && !empty($param['number'])) {
            $query->where(self::$_table_name.'.password',  \Lib\Util::encodePassword($param['password'], $param['number']));
        }
        $row = $query->execute()->offsetGet(0);
        if (empty($row)) {
            if (isset($param['id'])) {
                static::errorNotExist('id', $param['id']);
            }
            if (isset($param['number'])) {
                static::errorNotExist('number_or_password', $param['number']);
            }
        }
        if (isset($param['get_new_notification'])) {
            $row['total_new_notification'] = Model_Notice::count_new(array(
                'receive_user_id' => $row['id']               
            ));
            /*
            $option['where'] = array(
                'receive_user_id' => $row['id'],
                'is_read' => 0,
            );
            $row['total_new_notification'] = Model_Notice::count($option); */
        }
        return !empty($row)? $row : array();
    }
    
    /**
     * Function to get a list of user.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param) {
        $page = !empty($param['page']) ? $param['page'] : 1;
        $limit = !empty($param['limit']) ? $param['limit'] : 10;
        $offset = ($page - 1) * $limit;
        $query = DB::select(
                    array('users.id', 'id'),
                    'users.user_group_id', 'users.enrollment_id', 
                    array('user_groups.name', 'user_group_name'), array('enrollments.name', 'enrollment_name'),
                    'users.number', 'users.hash', array('users.name', 'user_name'), 'users.grade', 
                    'users.mail', 'users.mail_parent',
                    'users.last_login_date', 'users.memo', 'users.type',
                    DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url), IF(users.sex_id = 1, '" . \Config::get('no_image_user_male') . "', '".\Config::get('no_image_user_female')."')) AS image_url"),
                    'users.created', 'users.updated', 'users.disable'
                )
                ->from('users')
                ->join('user_groups','LEFT')
                ->on(self::$_table_name.'.user_group_id','=','user_groups.id')
                ->join('enrollments', 'LEFT')
                ->on(self::$_table_name.'.enrollment_id','=','enrollments.id')
                ->limit($limit)
                ->offset($offset);

        if (!empty($param['user_group_id'])) {
            $query->where(self::$_table_name.'.user_group_id', $param['user_group_id']);
        }
        if (!empty($param['enrollment_id'])) {
            $query->where(self::$_table_name.'.enrollment_id', $param['enrollment_id']);
        }
        if (!empty($param['number'])) {
            $query->where(self::$_table_name.'.number', $param['number']);
        }
        if (!empty($param['name'])) {
            $query->where(self::$_table_name.'.name', 'LIKE', "{$param['name']}%");
        }
        if (!empty($param['grade'])) {
            $query->where(self::$_table_name.'.grade', $param['grade']);
        }
        if (isset($param['sex_id']) && $param['sex_id'] != '') {
            $query->where(self::$_table_name.'.sex_id', $param['sex_id']);
        }
         if (!empty($param['mail'])) {
            $query->where(DB::expr("(users.mail LIKE  '%{$param['mail']}%' OR users.mail_parent LIKE '%{$param['mail']}%')"));
        }
         if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name.'.disable', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }
    
    /**
     * Function to add or update a user.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return int|bool Returns the integer or the boolean.
     */
    public static function add_update($param, $encodePwd = true) {
        //check id if existing
        $id = !empty($param['id']) ? $param['id'] : 0;
        $user = new self;
        if (!empty($id)) {
            $user = self::find($id);
            if (empty($user)) {
                static::errorNotExist('user_id', $param['id']);
                return false;
            }
        } elseif(!empty($param['number'])) {
            //Check Email in system
            $options['where'][] = array(
                'number' => $param['number']
            );
            $data = self::find('first', $options);
            if (!empty($data)) {
                static::errorDuplicate('number', $param['number']);
                return false;
            }
        }  
       
        // set information for user if having
        if (isset($param['user_group_id'])) {
            $user->set('user_group_id', $param['user_group_id']);
        }
        if (isset($param['enrollment_id'])) {
            $user->set('enrollment_id', $param['enrollment_id']);
        }
        if (isset($param['number'])) {
            $user->set('number', $param['number']);
        }
        if($encodePwd){
            if (isset($param['password'])) {
                $user->set('password', \Lib\Util::encodePassword($param['password'], $param['number']));
                $user->set('hash', \Lib\Util::encodePassword($param['password'], $param['number']));
            }
        }else{
            if (isset($param['password'])) {
                $user->set('password', $param['password']);
                $user->set('hash', $param['password']);
            }
        }
        if (isset($param['hash'])) {
            $user->set('hash', $param['hash']);
        }
        if (isset($param['name'])) {
            $user->set('name', $param['name']);
        }
        if (isset($param['grade'])) {
            $user->set('grade', $param['grade']);
        }
        if (isset($param['mail'])) {
            $user->set('mail', $param['mail']);
        }
        if (isset($param['mail_parent'])) {
            $user->set('mail_parent', $param['mail_parent']);
        }
        if (isset($param['last_login_date'])) {
            $user->set('last_login_date', strtotime($param['last_login_date']));
        }
        if (isset($param['memo'])) {
            $user->set('memo', $param['memo']);
        }
        if (isset($param['type']) && $param['type'] != '') {
            $user->set('type', $param['type']);
        }
         if (isset($param['image_url'])) {
            $user->set('image_url', $param['image_url']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $user->set('disable', $param['disable']);
        }
        if (isset($param['image_url'])) {
            $user->set('image_url', $param['image_url']);
        }

        // set information for user if having
        if (isset($param['sex_id']) && $param['sex_id'] != '') {
            $user->set('sex_id', $param['sex_id']);
        }
        //check id for adding new or updating
        if ($user->save()) {
            if (!empty($param['from_sh'])) {
                $result = self::get_detail(array('id' => $user->id));
                $result['token'] = \Model_Authenticate::addupdate(array(
                    'user_id' => $result['id'],
                    'regist_type' => 'user'
                ));
                return $result;
            }
            if (empty($user->id)) {
                $user->id = self::cached_object($user)->_original['id'];
            }else{
                return !empty($user->id) ? $user->id : 0;
            }
            }
        return false;
    }
    
    /**
     * Function to disable or enable a user.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $user = self::find($id);
            if (!empty($user)) {
                $user->set('disable', $param['disable']);
                if (!$user->update()) {
                    return false;
                }
            }else{
                self::errorNotExist('id', $id);
            }
            
        }
        return true;
    }
    
    /**
     * Count answer/question/question_favorite/question_nice/answer_nice by user_id.
     *
     * @author truongnn
     * @param array $param Input array.
     * @return array Returns the array.
     */
    public static function get_statistic($param) {
        $sql = "SELECT 
                    (SELECT count(id) FROM  answer_nices WHERE user_id = {$param['user_id']} AND disable = 0) AS  answer_like_count,
                    (SELECT count(id) FROM  question_nices WHERE user_id = {$param['user_id']} AND disable = 0) AS  question_like_count,
                    (SELECT count(id) FROM  question_favorites WHERE user_id = {$param['user_id']} AND disable = 0) AS  question_favorite_count,
                    (SELECT count(id) FROM  answers WHERE user_id = {$param['user_id']} AND disable = 0) AS  answer_count,
                    (SELECT count(id) FROM  questions WHERE user_id = {$param['user_id']} AND disable = 0) AS  question_count
                FROM DUAL";
        $query = DB::query($sql);
        $data = $query->execute()->as_array();
        return !empty($data) ? $data[0] : array();
    }
    
    /**
     * Get profile of user by user_id.
     *
     * @author truongnn
     * @param array $param Input array.
     * @return array Return information of user.
     */
    public static function get_profile($param) {
        //Get information of user by user_id.
        $profile = self::get_detail(array('id' => $param['user_id']));
        //Get list questions by user_id.
        $question_list = \Model_Question::get_list_for_profile(array('user_id' => $param['user_id']));
        //Statistic user by user_id.
        $statistic = self::get_statistic(array('user_id' => $param['user_id']));
        $user_profile = array();
        $user_profile['profile'] = $profile;
        $user_profile['question_list'] = $question_list;
        foreach ($statistic as $key => $value) {
            switch ($key) {
                case 'question_count':
                case 'question_favorite_count':
                case 'question_like_count':
                case 'answer_like_count':
                case 'answer_count':
                    $user_profile[$key] = $value;
                    break;
            }
        }
        return $user_profile;
    }
    
    
    /**
     * Get general report from user.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array data report.
     */
    public static function get_general_report($param)
    {   
        $str_question_pending = "SELECT count(id) FROM  questions WHERE status = 0 AND disable = 0". self::get_query_condition($param);
        $str_answer_pending = "SELECT count(id) FROM  answers WHERE status = 0 AND question_id > 0 AND disable = 0". self::get_query_condition($param);
        
        $sql = "SELECT 
                ({$str_question_pending}) AS question_pending_count, 
                ({$str_answer_pending}) AS answer_pending_count FROM DUAL;";
        
        $query = DB::query($sql);
        $data = $query->execute()->as_array();
        return !empty($data) ? $data[0] : array();
    }
    /**
     * Get query condition  by date_from/date_to.
     * 
     * @author truongnn
     * @param array $param Input array.
     * @return string String conditions
     */
    public static function get_query_condition($param){
        $str = '';
        if (!empty($param['date_from'])) {
            $str .= " AND created >= " .self::date_from_val($param['date_from']);
        }
        if (!empty($param['date_to'])) {
            $str .= " AND created <= " .self::date_from_val($param['date_to']);
        }
        return $str;
    }

    /**
     * Function login tadacopy by user.
     *
     * @author thailh
     * @param array $param Input data.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function tadacopy_user_login($param)
    {
        if (empty($param['tadacopy_app_id'])) {
            self::errorParamInvalid('tadacopy_app_id');
            return false;
        }
        $user = self::find('first', array(
            'where' => array(
                'app_id' => $param['tadacopy_app_id']
            )
        ));
        if ($user) {
            return $user->get('id');
        }
        $user = new self;
        $user->set('app_id', $param['tadacopy_app_id']);
        $user->set('user_group_id', '0');
        $user->set('enrollment_id', '0');
        $user->set('number', '0');
        $user->set('password', '');
        $user->set('name', 'tadacopy_user');
        $user->set('grade', '');
        $user->set('mail', '');
        $user->set('image_url', '');
        if ($user->create()) {
            $user->id = self::cached_object($user)->_original['id'];
            return !empty($user->id) ? $user->id : 0;
        }
        return false;
    }
}
