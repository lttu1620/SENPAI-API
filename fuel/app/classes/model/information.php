<?php
/**
 * Model to operate to Information's functions.
 *
 * @package Model
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Model_Information extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'title',
		'content',
		'started',
		'finished',
		'print',
		'segment_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'informations';
        
     /**
     * Get list information.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*'
        )->from(self::$_table_name);
        if (!empty($param['title'])) {
            $query->where(self::$_table_name .'.title', 'LIKE', "%{$param['title']}%");
        }
        if (!empty($param['content'])) {
            $query->where(self::$_table_name .'.content', 'LIKE', "%{$param['content']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name .'.disable', $param['disable']);
        }
         if (isset($param['print']) && $param['print'] != '') {
            $query->where(self::$_table_name .'.print', $param['print']);
        }
        if (!empty($param['started'])) {
            $query->where(self::$_table_name . '.started', '>=', self::date_from_val($param['started']));
        }
        if (!empty($param['finished'])) {
            $query->where(self::$_table_name . '.finished', '<=', self::date_to_val($param['finished']));
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        if (!isset($param['page'])) {
            return $data;
        }
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Enable/disable a or any information.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $information = self::find($id);            
            if ($information) {
                 $information->set('disable', $param['disable']);
                if (!$information->update()) {
                    return false;
                }
            }  else {
                 self::errorNotExist('id', $id);
            }
        }
        return true;
    }

    /**
     * Add or update info for information.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return int|bool Returns the integer or the boolean.
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $information = new self;
        if (!empty($id)) {
            $information = self::find($id);
            if (empty($information)) {
                return false;
            }
        }
        if (!empty($param['title'])) {
            $information->set('title', $param['title']);
        }
         if (!empty($param['content'])) {
            $information->set('content', $param['content']);
        }
        if (!empty($param['started'])) {
            $information->set('started', strtotime($param['started']));
        }
        if (!empty($param['finished'])) {
            $information->set('finished', strtotime($param['finished']));
        }
         if (isset($param['print']) && $param['print'] != '') {
            $information->set('print', $param['print']);
        }
         if (!empty($param['segment_id'])) {
            $information->set('segment_id', $param['segment_id']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $information->set('disable', $param['disable']);
        }
        if ($information->save()) {
            if (empty($information->id)) {
                $information->id = self::cached_object($information)->_original['id'];
            }
            return !empty($information->id) ? $information->id : 0;
        }
        return false;
    }
     /**
     * Get detail information.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns the array.
     */
     public static function get_detail($param) {
        $query = DB::select(
                self::$_table_name . '.*',
                array('segments.name', 'segment_name')
            )
            ->from(self::$_table_name)
            ->join('segments','LEFT')
            ->on(self::$_table_name.'.segment_id','=','segments.id')
            ->limit(1)
            ->offset(0);
        if (!empty($param['id'])) {
            $query->where(self::$_table_name . '.id', $param['id']);
        }

        $data = $query->execute()->offsetGet(0);
        if (empty($data)) {
            static::errorNotExist('id', $param['id']);
        }
        return $data;
    }
    
     /**
     * Get all informations.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_all($param)
    {   
        $query = DB::select(
            self::$_table_name . '.*',
            array('segments.name', 'segment_name')
        )
        ->from(self::$_table_name)
        ->join('segments','LEFT')
        ->on(self::$_table_name.'.segment_id','=','segments.id');
        $query->where('informations.disable', 0);
       
        $data = $query->execute()->as_array();
        
        return !empty($data)? $data : array();
    }
    
    /**
     * Print/unprint a or any information.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return bool True if execute successfully or otherwise.
     */
    public static function printed($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $information = self::find($id);            
            if ($information) {
                 $information->set('print', $param['print']);
                if (!$information->update()) {
                    return false;
                }
            }  else {
                 self::errorNotExist('id', $id);
            }
        }
        return true;
    }
}
