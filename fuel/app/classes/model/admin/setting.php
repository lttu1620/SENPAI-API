<?php

class Model_Admin_Setting extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'admin_id',
		'setting_id',
		'value',
		'disable',
		'created',
		'updated',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'admin_settings';

}
