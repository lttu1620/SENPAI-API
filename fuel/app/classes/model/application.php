<?php
/**
 * Model to operate to Application's functions.
 *
 * @package Model
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Model_Application extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'name',
		'url',
		'started',
		'finished',
		'priority',
		'segment_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'applications';
        
     /**
     * Get list application
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*'
        )
        ->from(self::$_table_name)
        ->join('segments','LEFT')
        ->on(self::$_table_name.'.segment_id','=','segments.id');
        if (!empty($param['name'])) {
            $query->where('applications.name', 'LIKE', "%{$param['name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where('applications.disable', $param['disable']);
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        if (!isset($param['page'])) {
            return $data;
        }
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

     /**
     * Get all applications.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_all($param)
    {   
        $query = DB::select(
            self::$_table_name . '.*',
            array('segments.name', 'segment_name')
        )
        ->from(self::$_table_name)
        ->join('segments','LEFT')
        ->on(self::$_table_name.'.segment_id','=','segments.id');
        $query->where('applications.disable', 0);
       
        $data = $query->execute()->as_array();
        
        return !empty($data)? $data : array();
    }
    
    /**
     * Enable/disable a or any application.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $application = self::find($id);            
            if ($application) {
                 $application->set('disable', $param['disable']);
                if (!$application->update()) {
                    return false;
                }
            }  else {
                 self::errorNotExist('id', $id);
            }
        }
        return true;
    }

    /**
     * Add or update info for application.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return int|bool Returns the integer or the boolean.
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $application = new self;
        if (!empty($id)) {
            $application = self::find($id);
            if (empty($application)) {
                return false;
            }
        }
        if (!empty($param['name'])) {
            $application->set('name', $param['name']);
        }
        if (!empty($param['url'])) {
            $application->set('url', $param['url']);
        }
        if (!empty($param['started'])) {
            $application->set('started', strtotime($param['started']));
        }
        if (!empty($param['finished'])) {
            $application->set('finished', strtotime($param['finished']));
        }
         if (!empty($param['priority'])) {
            $application->set('priority', $param['priority']);
        }
         if (!empty($param['segment_id'])) {
            $application->set('segment_id', $param['segment_id']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $application->set('disable', $param['disable']);
        }
        if ($application->save()) {
            if (empty($application->id)) {
                $application->id = self::cached_object($application)->_original['id'];
            }
            return !empty($application->id) ? $application->id : 0;
        }
        return false;
    }
     /**
     * Get detail application.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns the array.
     */
     public static function get_detail($param) {
        $query = DB::select(
                self::$_table_name . '.*',
                array('segments.name', 'segment_name')
            )
            ->from(self::$_table_name)
            ->join('segments','LEFT')
            ->on(self::$_table_name.'.segment_id','=','segments.id')
            ->limit(1)
            ->offset(0);
        if (!empty($param['id'])) {
            $query->where(self::$_table_name . '.id', $param['id']);
        }

        $data = $query->execute()->offsetGet(0);
        if (empty($data)) {
            static::errorNotExist('id', $param['id']);
        }
        return !empty($data) ? $data : array();
    }
    
    /**
     * Get list application by user_id.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list_by_user_id($param)
    {
        $query = DB::select(
            self::$_table_name . '.*'
        )
        ->from(self::$_table_name)
        ->join('segments','LEFT')
        ->on(self::$_table_name.'.segment_id','=','segments.id');
        if (!empty($param['user_id'])) {
            $query->where(DB::expr(" EXISTS (
                                            SELECT A.segment_id
                                            FROM segment_enrollments A JOIN segment_user_groups B 
                                                            ON A.segment_id = B.segment_id
                                            WHERE A.segment_id=applications.segment_id
                                            AND
                                            A.enrollment_id IN (SELECT enrollment_id FROM users WHERE id = {$param['user_id']}) 
                                            AND 
                                            B.user_group_id IN (SELECT user_group_id FROM users WHERE id = {$param['user_id']})
                                        ) "));
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where('applications.disable', $param['disable']);
        }
        
        $data = $query->execute()->as_array();
        return !empty($data) ? $data : array();
    }
}
