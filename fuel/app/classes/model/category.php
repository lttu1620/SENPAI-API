<?php

/**
 * Any query in Model Category
 *
 * @package Model
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Category extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'name',
        'image_url',
        'color',
        'sort',
        'disable',
        'created',
        'updated',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'categories';

    /**
     * Add or update info for Category
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Category id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $category = new self;
        // check exist
        if (!empty($id)) {
            $category = self::find($id);
            if (empty($category)) {
                self::errorNotExist('category_id');
                return false;
            }
        }
        // set value
        if (!empty($param['name'])) {
            $category->set('name', $param['name']);
        }
        if (!empty($param['image_url'])) {
            $category->set('image_url', $param['image_url']);
        }
        if (!empty($param['color'])) {
            $category->set('color', $param['color']);
        }
        if (!empty($param['sort'])) {
            $category->set('sort', $param['sort']);
        }
        // save to database
        if ($category->save()) {
            if (empty($category->id)) {
                $category->id = self::cached_object($category)->_original['id'];
            }
            return !empty($category->id) ? $category->id : 0;
        }
        return false;
    }

    /**
     * Get list Category (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List category
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get all Category (without array count)
     *
     * @author Le Tuan Tu
     * @modified CaoLP
     * @return array List category
     */
    public static function get_all($param)
    {
        $query = DB::select()
            ->from(self::$_table_name)
            ->where('disable', '=', '0');

        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Enable/disable list Category
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $category = self::find($id);
            if ($category) {
                $category->set('disable', $param['disable']);
                if (!$category->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('category_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Category
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail category or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('category_id');
            return false;
        }
        return $data;
    }

    /**
     * Update field question_count for Category
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function update_counter($param)
    {
        $idArray = explode(',', $param['category_id']);
        foreach ($idArray as $id) {
            $sql = "
                UPDATE categories
                SET    question_count = (SELECT COUNT(*)
                                         FROM   questions
                                         WHERE  category_id = {$id}
                                                AND disable = 0)
                WHERE  id = {$id}
                       AND disable = 0
            ";
            DB::query($sql)->execute();
        }
        return true;
    }
}
