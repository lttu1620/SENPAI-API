<?php

class Model_User_Activation extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'user_id',
		'email',
		'password',
		'token',
		'expire_date',
		'sex_id',
		'regist_type',
		'os',
		'disable',
		'created',
		'updated',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'user_activations';

}
