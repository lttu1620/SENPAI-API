<?php

/**
 * Any query in Model User Group
 *
 * @package Model
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_User_Group extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'name',
        'created',
        'updated',
        'disable'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'user_groups';

    /**
     * Add or update info for User Group
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool User group id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $group = new self;
        // check exist
        if (!empty($id)) {
            $group = self::find($id);
            if (empty($group)) {
                self::errorNotExist('user_group_id');
                return false;
            }
        }
        // set value
        if (!empty($param['name'])) {
            $group->set('name', $param['name']);
        }
        // save to database
        if ($group->save()) {
            if (empty($group->id)) {
                $group->id = self::cached_object($group)->_original['id'];
            }
            return !empty($group->id) ? $group->id : 0;
        }
        return false;
    }

    /**
     * Get list User Group (using for admin page)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List user group
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get all User Group (using for mobile)
     *
     * @author Le Tuan Tu
     * @return array List user group
     */
    public static function get_all()
    {
        $query = DB::select()
            ->from(self::$_table_name);
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Get detail User Group
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail user group or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('user_group_id');
            return false;
        }
        return $data;
    }
    
    /**
     * Enable/disable user_groups.
     *
     * @author truongnn
     * @param array array $param Input data.
     * @return bool True if disable successfully or otherwise.
     */
    public static function disable($param)
    {
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $user_group = self::find($id);
            if ($user_group) {
                $user_group->set('disable', $param['disable']);
                if (!$user_group->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('user_group_id');
                return false;
            }
        }

        return true;
    }
}
