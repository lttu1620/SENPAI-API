<?php
/**
 * Any query in Model Notice.
 *
 * @package Model
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Model_Notice extends Model_Abstract
{
	protected static $_properties = array(
		'id',
        'receive_user_id',
		'question_id',
		'answer_id',
		'user_id',
		'type',
        'is_read',
		'created',
		'updated',
		'disable',
	);

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $not_checks = array('id', 'created', 'updated');

    protected static $like_search = array('question_id', 'answer_id', 'user_id', 'type');

    protected static $_table_name = 'notices';
        
     /**
     * Get list notice.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        if (empty($param['page'])) {
            $param['page'] = 1;
        }
        $query = DB::select(
            self::$_table_name . ".*",
            array('users.name', 'user_name'),
            DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url), IF(users.sex_id = 1, '" . \Config::get('no_image_user_male') . "', '".\Config::get('no_image_user_female')."')) AS user_image_url"),
            array('questions.content', 'questions_content'),
            array('questions.answer_count', 'answer_count'),
            array('questions.nice_count', 'nice_count'),
            array('questions.favorite_count', 'favorite_count'),
            array('answers.content', 'answers_content'),
            array('notice_user_counts.user_count', 'user_count')
        )
            ->from(self::$_table_name)
            ->join(DB::expr("
                (SELECT receive_user_id, question_id, answer_id, type, max(updated) AS updated
                FROM notices
                GROUP BY receive_user_id, question_id, answer_id, type ) B
            "))
            ->on(self::$_table_name . '.receive_user_id', '=', 'B.receive_user_id')
            ->on(self::$_table_name . '.question_id', '=', 'B.question_id')
            ->on(self::$_table_name . '.answer_id', '=', 'B.answer_id')
            ->on(self::$_table_name . '.type', '=', 'B.type')
            ->on(self::$_table_name . '.updated', '=', 'B.updated')
            ->join('users')
            ->on('users.id', '=', self::$_table_name . '.user_id')
            ->join('questions', 'LEFT')
            ->on('questions.id', '=', self::$_table_name . '.question_id')
            ->join('answers', 'LEFT')
            ->on('answers.id', '=', self::$_table_name . '.answer_id')
            ->join('notice_user_counts', 'LEFT')
            ->on('notice_user_counts.question_id', '=', self::$_table_name . '.question_id');
        foreach ($param as $key => $val) {
            if (in_array($key, self::$_properties) && !in_array($key, self::$not_checks)) {
                if ($val != '') {
                    if (in_array($key, self::$like_search)) {
                        $query->where($key, 'LIKE', "{$val}%");
                    } else {
                        $query->where($key, $val);
                    }
                }
            }
        }
        if (!empty($param['login_user_id'])) {
            $query->where(self::$_table_name . '.receive_user_id', '=', $param['login_user_id']);
        }  
        if (!empty($param['number'])) {
            $query->where('users.number', '=', $param['number']);
        }
        if (isset($param['is_read'])) {
            $query->where(self::$_table_name . '.is_read', '=', $param['is_read']);
        }
        $query->where(self::$_table_name . '.disable', '=', 0);
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
            $query->order_by(self::$_table_name . '.updated', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = 0;

        if(!empty($data)){
            $total =  DB::count_last_query();
            foreach (@$data as $index => $item) {
                switch (intval($item['type'])) {
                    case 1:
                        # Get list user anser your questions by question_id
                       
                        break;
                    
                    case 2:
                        # Get list user favorite your questions.
                         /*$sql = "";
                        if(empty($item['answer_id'])){
                            $sql ="
                                select count(*) as like_number
                                from question_favorites 
                                where question_favorites.question_id = ".$item['question_id'] ."
                            ";
                        }
                        if(!empty($sql)){
                             $query = DB::query($sql);
                            $favorite_number = $query->execute()->as_array();
                            $data[$index]['favorite_number'] = !empty($favorite_number)? $favorite_number[0]['like_number']: 0;
                        }else{
                             $data[$index]['favorite_number'] = $item['favorite_count'];
                        }
                       */
                        break;
                    
                    case 3:
                        # get list user like your questions or answers
                        /*$sql = "";
                        if(empty($item['answer_id'])){
                            $data[$index]['answer_id'] = 0;
                            $sql ="
                                select count(*) as like_number
                                from question_nices
                                where question_nices.question_id = ".$item['question_id'] ."
                            ";
                        }else{
                            $data[$index]['answer_id'] = $item['answer_id'];
                             $sql ="
                                select count(*) as like_number
                                from answer_nices
                                where answer_nices.answer_id = ".$item['answer_id'] ."
                            ";
                        }
                        if(!empty($sql)){
                            $query = DB::query($sql);
                            $like_number = $query->execute()->as_array();
                            $data[$index]['like_number'] = $like_number ? $like_number[0]['like_number']: 0;
                        }else{
                            $data[$index]['like_number'] =  0;
                        }
                        */
                        break;
                    
                    case 4:
                        # Get list user answer your favorite question
                        // Check user has answer in answers table
                        $sql ="
                                select count(*) as is_exists
                                from answers
                                where answers.user_id = ".$item['receive_user_id'] ."
                            ";
                        $query = DB::query($sql);
                        $is_exists = $query->execute()->as_array();
                        $is_exists = !empty($is_exists)? $is_exists[0]['is_exists'] :0;
                        if(!empty($data[$index]['user_count']) && intval($is_exists) > 0){
                            $data[$index]['user_count'] = (intval($data[$index]['user_count']) - 1);
                        }
                        break;
                    
                    case 5:
                        # get list user answer the question that you answer.
                        //user_count
                        if(!empty($data[$index]['user_count'])){
                            $data[$index]['user_count'] = (intval($data[$index]['user_count']) - 1);
                        }
                        break;
                }
            }
        }
        return array($total, $data);   
    }

    /**
     * Enable/disable a or some notice.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return bool True if disable successfully or otherwise.
     */
    public static function disable($param)
    {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $notice = self::find($id);
            $notice->set('disable', $param['disable']);
            if (!$notice->save()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check read for Notices
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function isRead($param)
    {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {           
            $notice = self::find($id);
            if (!empty($notice)) {
                $sql = "
                    UPDATE  notices
                    SET     is_read = 1
                    WHERE   receive_user_id = '{$notice->receive_user_id}'
                            AND question_id = '{$notice->question_id}'
                            AND answer_id = '{$notice->answer_id}'
                            AND type = '{$notice->type}'
                            AND is_read = 0
                ";
                DB::query($sql)->execute();
            }            
        }
        return true;
    }

    /**
     * Add info for notice.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return int|bool false if add/update not successful or value id of notice.
     */
    public static function add($param)
    {
        // get list recipient
        $recipient = array();
        if ($param['type'] == Config::get('type_notice')['Answer_MyQuestion'] ||
            $param['type'] == Config::get('type_notice')['Favorite_MyQuestion']
        ) { // anyone answer or favorite my question
            $question = Model_Question::get_detail(array(
                'id' => $param['question_id']
            ));
            if ($question) {
                $recipient[] = $question['user_id'];
            } else {
                return false;
            }
        } elseif ($param['type'] == Config::get('type_notice')['Like_MyAnswerQuestion']) {
            if (!empty($param['answer_id'])) { // anyone like my answer
                $answer = Model_Answer::get_detail(array(
                    'id' => $param['answer_id']
                ));
                if ($answer) {
                    $recipient[] = $answer['user_id'];
                } else {
                    return false;
                }
            } else { // anyone like my question
                $question = Model_Question::get_detail(array(
                    'id' => $param['question_id']
                ));
                if ($question) {
                    $recipient[] = $question['user_id'];
                } else {
                    return false;
                }
            }
        } elseif ($param['type'] == Config::get('type_notice')['Answer_MyFavoriteQuestion']) {
            // anyone answer the question that I have favorite
            $listFavorites = Model_Question_Favorite::get_list(array(
                'question_id'     => $param['question_id'],
                'exclude_user_id' => $param['user_id']
            ));
            $recipient = \Lib\Arr::field($listFavorites[1], 'user_id');

        } elseif ($param['type'] == Config::get('type_notice')['Answer_MyAnswerQuestion']) {
            // anyone answer the question that I have answer
            $listAnswer = Model_Answer::get_list(array(
                'question_id'     => $param['question_id'],
                'exclude_user_id' => $param['user_id']
            ));
            $recipient = \Lib\Arr::field($listAnswer[1], 'user_id');
        }
        // add to database
        $params = array();
        foreach ($recipient as $userId) {
            $notice = new self;
            $param['receive_user_id'] = $userId;
            //set information
            foreach ($param as $key => $val) {
                if (in_array($key, self::$_properties) && !in_array($key, self::$not_checks)) {
                    if ($val != '') {
                        $notice->set($key, $val);
                    }
                }
            }
            if (empty($param['answer_id'])) {
                $param['answer_id'] = 0;
            }
            $params[] = $param;
            /*
            if (!$notice->create()) {
                return false;
            }
            * 
            */
            unset($param['receive_user_id']);
        }
        if (!empty($params)) {
            return parent::batchInsert(
                self::$_table_name, 
                $params, 
                array(
                    'user_id' => $param['user_id'],
                    'answer_id' => $param['answer_id'],
                    'disable' => '0',
                    'is_read' => '0',
                ), 
                false
            );
        }
        return true;
    }
   
    /**
     * Get all notices.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array List notices.
     */
     public static function get_all($param)
    {   
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'user_name'),
            array('questions.content', 'question_content'),
            array('answers.content', 'answer_content')
        )
        ->from(self::$_table_name)
        ->join('questions', 'LEFT')
        ->on(self::$_table_name . '.question_id', '=', 'questions.id')
        ->join('answers', 'LEFT')
        ->on(self::$_table_name . '.answer_id', '=', 'answers.id')
        ->join('users', 'LEFT')
        ->on(self::$_table_name . '.user_id', '=', 'users.id');
        $query->where(self::$_table_name .'.disable', 0);
       
        $data = $query->execute()->as_array();
        
        return !empty($data)? $data : array();
    }
    
     /**
     * Get detail notice.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns the array.
     */
     public static function get_detail($param) {
        $query = DB::select(
                self::$_table_name . '.*',
                array('users.name', 'user_name'),
                array('questions.content', 'question_content'),
                array('answers.content', 'answer_content')
            )
            ->from(self::$_table_name)
                ->join('questions', 'LEFT')
                ->on(self::$_table_name . '.question_id', '=', 'questions.id')
                ->join('answers', 'LEFT')
                ->on(self::$_table_name . '.answer_id', '=', 'answers.id')
                ->join('users', 'LEFT')
                ->on(self::$_table_name . '.user_id', '=', 'users.id')
                ->limit(1)
                ->offset(0);
        if (!empty($param['id'])) {
            $query->where(self::$_table_name . '.id', $param['id']);
        }

        $data = $query->execute()->offsetGet(0);
        if (empty($data)) {
            static::errorNotExist('id', $param['id']);
        }
        return $data;
    }
    
    public static function count_new($param)
    {
        $query = DB::select(
            DB::expr("COUNT(*) AS cnt")            
        )       
        ->from(DB::expr("
            (   SELECT receive_user_id, question_id, answer_id, type
                FROM notices
                WHERE receive_user_id = '{$param['receive_user_id']}'
                    AND is_read = 0
                    AND disable = 0
                GROUP BY receive_user_id, question_id, answer_id, type                 
            ) B
        "));
        $data = $query->execute()->offsetGet(0);
        return !empty($data['cnt']) ? $data['cnt'] : 0;
    }
    
    public static function answerItem($row, $msg) {
        return $row['user_name'] . $msg;
    }
     
    public static function favoriteItem($row) {
        if (intval($row['favorite_count']) > 1) {
            return $row['user_name'] . 'さんと他' . (intval($row['favorite_count']) - 1) . '人があなたの質問を知りたい！しました。';
        } else {
            return $row['user_name'] . 'さんがあなたの質問を知りたい！しました。';
        }
    }
    
    public static function likeItem($row) {
        if (intval($row['nice_count']) > 1) {
            return $row['user_name'] . 'さんと他' . (intval($row['nice_count']) - 1) . '人があなたの質問をいいね！しました。';
        } else {
            return $row['user_name'] . 'さんがあなたの回答をいいね！しました。';
        }
    }
    
    /**
     * Get list notice.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_for_smarthighschool($param)
    {       
        if (empty($param['limit'])) {
            $param['limit'] = 20;
        }
        $param['is_read'] = 0;
        $param['disable'] = 0;
        list($total, $data) = self::get_list($param);
        $notices = array();
        foreach ($data as $row) {
            $notice = '';
            switch ($row['type']) {
                case '1':
                    if (intval($row['user_count']) > 1) {
                        $msg = "さんと他" . (intval($row['user_count']) - 1 ) . "人があなたの質問に回答しました。";
                    } else {
                        $msg = "さんがあなたの質問に回答しました。";
                    }
                    $notice = self::answerItem($row, $msg);
                    break;
                case '4':
                    if (intval($row['user_count']) > 1) {
                        $msg = "さんと他" . (intval($row['user_count']) - 1 ) . "人が私も知りたい！をした質問に回答しました。";
                    } else {
                        $msg = "さんが私も知りたい！をした質問に回答しました。";
                    }
                    $notice = self::answerItem($row, $msg);
                    break;
                case '5':
                    // case answer
                    if (intval($row['user_count']) > 1) {
                        $msg = "さんと他" . (intval($row['user_count']) - 1) . "人があなたが参加をした質問に回答しました。";
                    } else {
                        $msg = "さんがあなたが参加をした質問に回答しました。";
                    }
                    $notice = self::answerItem($row, $msg);
                    break;
                case '2':
                    // favorite 
                    $notice = self::favoriteItem($row);
                    break;
                case '3':
                    // like
                    $notice = self::likeItem($row);
                    break;
            }
            $notices[] = array(
                'type' => $row['type'],
                'user_id' => $row['user_id'],
                'user_name' => $row['user_name'],
                'user_image_url' => $row['user_image_url'],                
                'question_id' => $row['question_id'],
                'questions_content' => $row['questions_content'],
                'answer_count' => $row['answer_count'],
                'nice_count' => $row['nice_count'],
                'favorite_count' => $row['favorite_count'],
                'answer_id' => $row['answer_id'],
                'answers_content' => $row['answers_content'],
                'user_count' => $row['user_count'],
                'notice' => $notice,
                'date' => date('Y-m-d H:i:s', $row['created']),
            );
        }
        return array(
            'unreads_number' => $total, 
            'notices' => $notices
        );
    }
    
}
