<?php
/**
 * Model to operate to Enrollment's functions.
 *
 * @package Model
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Model_Enrollment extends Model_Abstract
{
	protected static $_properties = array(
		'id',
		'name',
		'started',
		'finished',
                'disable',
		'created',
		'updated',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'enrollments';
     /**
     * Get list enrollment
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*'
        )->from(self::$_table_name);
        if (!empty($param['name'])) {
            $query->where('enrollments.name', 'LIKE', "%{$param['name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where('enrollments.disable', $param['disable']);
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        if (!isset($param['page'])) {
            return $data;
        }
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Enable/disable a or any enrollment.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param) {
        if (empty($param['id'])) {
            return false;
        }
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $enrollment = self::find($id);            
            if ($enrollment) {
                 $enrollment->set('disable', $param['disable']);
                if (!$enrollment->update()) {
                    return false;
                }
            }  else {
                 self::errorNotExist('id', $id);
            }
        }
        return true;
    }

    /**
     * Add or update info for enrollment.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return int|bool Returns the integer or the boolean.
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $enrollment = new self;
        if (!empty($id)) {
            $enrollment = self::find($id);
            if (empty($enrollment)) {
                return false;
            }
        }
        if (!empty($param['name'])) {
            $enrollment->set('name', $param['name']);
        }
        if (!empty($param['started'])) {
            $enrollment->set('started', strtotime($param['started']));
        }
        if (!empty($param['finished'])) {
            $enrollment->set('finished', strtotime($param['finished']));
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $enrollment->set('disable', $param['disable']);
        }
        if ($enrollment->save()) {
            if (empty($enrollment->id)) {
                $enrollment->id = self::cached_object($enrollment)->_original['id'];
            }
            return !empty($enrollment->id) ? $enrollment->id : 0;
        }
        return false;
    }
     /**
     * Get detail enrollment.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns the array.
     */
     public static function get_detail($param) {
        $query = DB::select(
                self::$_table_name . '.*'
            )
            ->from(self::$_table_name)
            ->limit(1)
            ->offset(0);
        if (!empty($param['id'])) {
            $query->where(self::$_table_name . '.id', $param['id']);
        }

        $data = $query->execute()->offsetGet(0);
        if (empty($data)) {
            static::errorNotExist('id', $param['id']);
        }
        return $data;
    }
}
