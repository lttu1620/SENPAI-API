<?php

class Model_Violation_Report extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'user_id',
        'question_id',
        'answer_id',
        'created',
        'updated',
        'disable',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'violation_reports';

    /**
     * Add info for Question Favorite
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return int|bool Question Favorite id or false if error
     *
     */
    public static function add($param) {
       
        if (empty($param['question_id']) || empty($param['user_id'])) {
            return false;
        }
        
        if(!empty($param['question_id'])){
             $question = \Model_Question::find($param['question_id']);
            if (empty($question)) {
                static::errorNotExist('question_id', $param['question_id']);
                return false;
            }
        } elseif(!empty($param['user_id'])){
             $user = \Model_User::find($param['user_id']);
            if (empty($user)) {
                static::errorNotExist('user_id', $param['user_id']);
                return false;
            }
        }
        $answer_id = empty($param['answer_id']) ? 0 : $param['answer_id'];
        //Check exist report.
        $condition['where'][] = array(
            'user_id'       => $param['user_id'],
            'question_id'   => $param['question_id'],
            'answer_id'     => $answer_id
        );
        $report = \Model_Violation_Report::find('first', $condition);
        if(!empty($report)){
             static::errorNotExist('violation_report_id', $report->get('id'));
                return false;
        }
        $valition_report = new self;
        if (!empty($param['question_id'])) {
            $valition_report->set('question_id', $param['question_id']);
        }
        if (!empty($param['user_id'])) {
            $valition_report->set('user_id', $param['user_id']);
        }
        $valition_report->set('answer_id', $answer_id);
        //check id for adding new or updating
        if ($valition_report->save()) {
            if (empty($valition_report->id)) {
                $user->id = self::cached_object($valition_report)->_original['id'];
            }
            return !empty($valition_report->id) ? $valition_report->id : 0;
        }
        return false;
    }

}
