<?php

/**
 * Any query in Model Answer
 *
 * @package Model
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Answer extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'user_id',
        'question_id',
        'content',
        'nice_count',
        'disable',
        'created',
        'updated',
        'status'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'answers';

    /**
     * Add info for Answer
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Answer id or false if error
     */
    public static function add($param)
    {
        $answer = new self;
        // set value
        $answer->set('user_id', $param['user_id']);
        $answer->set('question_id', $param['question_id']);
        $answer->set('content', $param['content']);
        if(isset($param['status']))
            $answer->set('status', $param['status']);
        else
            $answer->set('status', 0);
        // add to database
        if ($answer->create()) {
            $answer->id = self::cached_object($answer)->_original['id'];
            if (isset($param['medias'])) {
                \LogLib::error($param['medias']);
                $medias = json_decode($param['medias'],true);
                foreach ($medias as $media) {
                    $save_params = array(
                        'user_id'     => $param['user_id'],
                        'answer_id' => $answer->id,
                        'media_url'   => $media['url'],
                        'media_name'   => $media['name'],
                        'type'   => $media['type']
                    );
                    Model_Answer_Media::add($save_params);
                }
            }
            return $answer->id;
        }
        return false;
    }

    /**
     * Update info for Answer
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function set_update($param)
    {
        // check exist
        $answer = self::find($param['id']);
        if (empty($answer)) {
            self::errorNotExist('answer_id');
            return false;
        }
        // set value
        if (!empty($param['user_id'])) {
            $answer->set('user_id', $param['user_id']);
        }
        if (!empty($param['question_id'])) {
            $answer->set('question_id', $param['question_id']);
        }
        if (!empty($param['content'])) {
            $answer->set('content', $param['content']);
        }
        // save to database
        if ($answer->save()) {
            return true;
        }
        return false;
    }

    /**
     * Get list Answer (using for admin page)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List answer
     */
    public static function get_list($param)
    {
        $param['login_user_id'] = !empty($param['login_user_id']) ? $param['login_user_id'] : 0;
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'user_name'),
            'users.sex_id',    
            array('questions.content', 'question_content'),
            DB::expr("
                IF(ISNULL(answer_nices.answer_id),0,1) AS is_like
            "),
            DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url), IF(users.sex_id = 1, '" . \Config::get('no_image_user_male') . "', '".\Config::get('no_image_user_female')."')) AS user_image_url")
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('questions')
            ->on(self::$_table_name . '.question_id', '=', 'questions.id')
            ->join(DB::expr("
                (SELECT *
                FROM answer_nices
                WHERE disable = 0 AND user_id = {$param['login_user_id']}) answer_nices
            "), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'answer_nices.answer_id');
        // filter by keyword
        if (!empty($param['question_id'])) {
            $query->where('question_id', '=', $param['question_id']);
        }
        if (!empty($param['user_id'])) {
            $query->where('users.id', '=', $param['user_id']);
        }
        if (!empty($param['exclude_user_id'])) {
            $query->where(self::$_table_name . '.user_id', '<>', $param['exclude_user_id']);
        }
        if (!empty($param['user_number'])) {
            $query->where('users.number', '=', $param['user_number']);
        }
        if (!empty($param['question_content'])) {
             $query->where('questions.content', 'LIKE', "%{$param['question_content']}%");
        }
        if (!empty($param['content'])) {
             $query->where(self::$_table_name.'.content', 'LIKE', "%{$param['content']}%");
        }
        if (!empty($param['report'])) {
            $query->where( DB::expr("EXISTS (SELECT id FROM violation_reports WHERE answer_id = answers.id  AND disable = 0)"));
        } 
        if (isset($param['disable']) && $param['disable'] !== '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (isset($param['status']) && $param['status'] !== '') {
            $query->where(self::$_table_name . '.status', '=', $param['status']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        
        $medias = \Model_Answer_Media::get_all_by_answer(array('answer_id' => \Lib\Arr::field($data, 'id')));       
        foreach ($data as &$row){
            $row['medias'] = !empty($medias[$row['id']]) ? ($medias[$row['id']]) : array();             
        }
        if (isset($row)) {
            unset($row);
        }
        
        return array($total, $data);
    }

    /**
     * Get all Answer (using for mobile)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List answer
     */
    public static function get_all($param)
    {
        $param['login_user_id'] = !empty($param['login_user_id']) ? $param['login_user_id'] : 0;
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'user_name'),
            'users.sex_id',
            array('questions.content', 'question_content'),
            DB::expr("
                IF(ISNULL(answer_nices.answer_id),0,1) AS is_like
            "),
            array('users.image_url', 'user_image_url')
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('questions')
            ->on(self::$_table_name . '.question_id', '=', 'questions.id')
            ->join(DB::expr("
                (SELECT *
                FROM answer_nices
                WHERE disable = 0 AND user_id = {$param['login_user_id']}) answer_nices
            "), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'answer_nices.answer_id')
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where('users.disable', '=', '0')
            ->where('questions.disable', '=', '0');
        // filter by keyword
        if (!empty($param['question_id'])) {
            $query->where('question_id', '=', $param['question_id']);
        }
        if (!empty($param['user_number'])) {
            $query->where('users.number', '=', $param['user_number']);
        }
        if (isset($param['status']) && $param['status'] != '') {
            $query->where(self::$_table_name . '.status', '=', $param['status']);
        }
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Get detail Answer
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail answer or false if error
     */
    public static function get_detail($param)
    {
        $param['login_user_id'] = !empty($param['login_user_id']) ? $param['login_user_id'] : 0;
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'user_name'),
            'users.sex_id',
            array('questions.content', 'question_content'),
            DB::expr("
                IF(ISNULL(answer_nices.answer_id),0,1) AS is_like
            "),
            DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url), IF(users.sex_id = 1, '" . \Config::get('no_image_user_male') . "', '".\Config::get('no_image_user_female')."')) AS user_image_url"),
            array('questions.content', 'question_content')
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('questions')
            ->on(self::$_table_name . '.question_id', '=', 'questions.id')
            ->join(DB::expr("
                (SELECT *
                FROM answer_nices
                WHERE disable = 0 AND user_id = {$param['login_user_id']}) answer_nices
            "), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'answer_nices.answer_id')
            ->where(self::$_table_name . '.id', '=', $param['id']);
        // get data
        $data = $query->execute()->offsetGet(0);
        if (empty($data)) {
            static::errorNotExist('answer_id');
            return false;
        }
        $data['answer_count'] = \Model_Question::get_count_answer($data['question_id']);
        $data['medias'] = \Model_Answer_Media::get_all(array('answer_id'=>$data['id']));
        return $data;
    }

    /**
     * Update field nice_count for Answer
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function update_counter($param)
    {
        $idArray = explode(',', $param['answer_id']);
        foreach ($idArray as $id) {
            $sql = "
                UPDATE answers
                SET    nice_count = (SELECT COUNT(*)
                                     FROM   answer_nices
                                     WHERE  answer_id = {$id}
                                            AND disable = 0)
                WHERE  id = {$id}
                       AND disable = 0
            ";
            DB::query($sql)->execute();
        }
        return true;
    }
    
    /**
     * Enable/disable list Answer
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $answer = self::find($id);
            if ($answer) {
                $answer->set('disable', $param['disable']);
                if (!$answer->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('answer_id');
                return false;
            }
        }
        return true;
    }
    
    /**
     * Approve list Answers.
     *
     * @author truongnn
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function approve($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $answer = self::find($id);
            if ($answer) {
                $param['question_id'] = $answer->question_id;
                $param['user_id'] = $answer->user_id;
                if ($answer->status != '1') {
                    $answer->set('status', 1);
                    if (!$answer->save()) {
                        return false;
                    }
                    // notice for user that created this question
                    if (!Model_Notice::add(array(
                        'question_id' => $param['question_id'],
                        'user_id'     => $param['user_id'],
                        'type'        => Config::get('type_notice')['Answer_MyQuestion'],
                        'answer_id'   => $id,
                        'is_read'     => '0'
                    ))) {
                        return false;
                    }
                    // notice for users that have favorite this question
                    if (!Model_Notice::add(array(
                        'question_id' => $param['question_id'],
                        'user_id'     => $param['user_id'],
                        'type'        => Config::get('type_notice')['Answer_MyFavoriteQuestion'],
                        'answer_id'   => $id,
                        'is_read'     => '0'
                    ))) {
                        return false;
                    }
                    // notice for users that have answer this question
                    if (!Model_Notice::add(array(
                        'question_id' => $param['question_id'],
                        'user_id'     => $param['user_id'],
                        'type'        => Config::get('type_notice')['Answer_MyAnswerQuestion'],
                        'answer_id'   => $id,
                        'is_read'     => '0'
                    ))) {
                        return false;
                    }
                }
            } else {
                self::errorNotExist('answer_id');
                return false;
            }
        }
        return true;
    }
    
    /**
     * Reject list Answers.
     *
     * @author truongnn
     * @param array $param Input array.
     * @return bool Success or otherwise.
     */
    public static function reject($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $answer = self::find($id);
            if ($answer) {
                $answer->set('status', 2);
                if (!$answer->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('answer_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get all Answer by list question id
     *
     * @author Le Tuan Tu
     * @param array $param Input array.
     * @return array List answer;
     */
    public static function get_all_by_question($param)
    {
        $result = array();
        if ($param['question_id']) {
            $query = DB::select(
                array('users.id', 'user_id'),
                array('users.name', 'user_name'),
                array('users.image_url', 'icon'),
                self::$_table_name . '.id',
                self::$_table_name . '.content',
                self::$_table_name . '.question_id',
                array(self::$_table_name . '.nice_count', 'nice'),
                self::$_table_name . '.created'
            )
                ->from(self::$_table_name)
                ->join('users')
                ->on(self::$_table_name . '.user_id', '=', 'users.id')
                ->where(self::$_table_name . '.disable', '=', '0')
                ->where('users.disable', '=', '0');
            if (!empty($param['question_id'])) {
                $query->where('question_id', 'IN', $param['question_id']);
            }
            $query->order_by(self::$_table_name . '.created', 'DESC');
            $rows = $query->execute()->as_array();
            if (!empty($rows)) {
                foreach ($rows as $row) {
                    if (!isset($result[$row['question_id']])) {
                        $result[$row['question_id']] = array();
                    }
                    $templateRow = $row;
                    unset($templateRow['question_id']);
                    $result[$row['question_id']][] = $templateRow;
                }
            }
        }
        return $result;
    }

    /**
     * Add info for Answer for tadacopy
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Answer id or false if error
     */
    public static function add_for_tadacopy($param)
    {
        $answer = new self;
        // set value
        $answer->set('user_id', $param['user_id']);
        $answer->set('question_id', $param['question_id']);
        $answer->set('content', $param['answer']);
        $answer->set('status', 0);
        // add to database
        if ($answer->create()) {
            $answer->id = self::cached_object($answer)->_original['id'];
            return $answer->id;
        }
        return false;
    }
}
