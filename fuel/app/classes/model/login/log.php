<?php
/**
 * Model to operate to Login_log's functions.
 *
 * @package Model
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Model_Login_Log extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'user_id',
		'created',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'login_logs';
        /**
	 * Get list login logs.
	 *
	 * @author truongnn
	 * @param array $param Input data.
	 * @return array Returns array(total, data).
	 */
	public static function get_list($param)
	{
		$query = DB::select(
			self::$_table_name . '.*',
			array('users.name', 'user_name'),
			array('users.number', 'user_number'),
			array('users.mail', 'user_mail')
		)
			->from(self::$_table_name)
			->join('users', 'LEFT')
			->on(self::$_table_name . '.user_id', '=', 'users.id');
			

		if (!empty($param['user_id'])) {
			$query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
		}
		if (!empty($param['user_number'])) {
			$query->where('users.number', $param['user_number']);
		}
		if (!empty($param['mail'])) {
			$query->where('users.mail', 'LIKE', "%{$param['mail']}%");
		}
		if (!empty($param['date_from'])) {
			$query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
		}
		if (!empty($param['date_to'])) {
			$query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
		}
		if (!empty($param['sort'])) {
			$sortExplode = explode('-', $param['sort']);
			if ($sortExplode[0] == 'created') {
				$sortExplode[0] = self::$_table_name . '.created';
			}
			$query->order_by($sortExplode[0], $sortExplode[1]);
		} else {
			$query->order_by(self::$_table_name . '.created', 'DESC');
		}
		if (!empty($param['page']) && !empty($param['limit'])) {
			$offset = ($param['page'] - 1) * $param['limit'];
			$query->limit($param['limit'])->offset($offset);
		}
		$data = $query->execute()->as_array();
		$total = !empty($data) ? DB::count_last_query() : 0;

		return array($total, $data);
	}

	/**
	 * Add info for login logs.
	 *
	 * @author truongnn
	 * @param array $param Input data.
	 * @return bool|int Returns the booleans or integer.
	 */
	public static function add($param)
	{
		$log = new self;
		$log->set('user_id', $param['user_id']);
		if ($log->create()) {
			$log->id = self::cached_object($log)->_original['id'];

			return !empty($log->id) ? $log->id : 0;
		}

		return false;
	}
}
