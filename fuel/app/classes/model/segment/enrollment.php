<?php

class Model_Segment_Enrollment extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'segment_id',
		'enrollment_id',
		'created',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'segment_enrollments';

}
