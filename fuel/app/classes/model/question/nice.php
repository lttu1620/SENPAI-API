<?php

/**
 * Any query in Model Question Nice
 *
 * @package Model
 * @created 2015-03-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Question_Nice extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'user_id',
        'question_id',
        'disable',
        'created',
        'updated',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'question_nices';

    /**
     * Add info for Question Nice
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return int|bool Question Nice id or false if error
     *
     */
    public static function add($param)
    {
        $query = DB::select(
            array('questions.id', 'question_id'),
            array('questions.nice_count', 'nice_count'),
            array('question_nices.id', 'nice_id'),
            array('question_nices.user_id', 'user_id'),
            array('question_nices.disable', 'nice_disable')
        )
            ->from('questions')
            ->join(
                DB::expr(
                    "(SELECT * FROM question_nices
                     WHERE user_id = {$param['user_id']}) AS question_nices"
                ),
                'LEFT'
            )
            ->on('questions.id', '=', 'question_nices.question_id')
            ->where('questions.id', '=', $param['question_id'])
            ->where('questions.disable', '=', '0');
        $data = $query->execute()->offsetGet(0);
        $status = false;
        if (empty($data['question_id'])) {
            return array(
                'status' => $status,
                'count'  => 0
            );
        }
        if (!empty($data['user_id']) && $data['nice_disable'] == 0) {
            //static::errorDuplicate('user_id');
            return array(
                'status' => $status,
                'count'  => $data['nice_count']
            );
        }
        $new = false;
        if (!empty($data['user_id']) && $data['nice_disable'] == 1) {
            $status = true;
            $dataUpdate = array(
                'id'      => $data['nice_id'],
                'disable' => '0'
            );
        } else {
            $new = true;
            $status = true;
            $dataUpdate = array(
                'question_id' => $data['question_id'],
                'user_id'     => $param['user_id']
            );
        }
        $nice = new self($dataUpdate, $new);
        if ($nice->save()) {
            // notice for user that created this question
            if (!Model_Notice::add(array(
                'question_id' => $param['question_id'],
                'user_id'     => $param['user_id'],
                'type'        => Config::get('type_notice')['Like_MyAnswerQuestion'],
                'is_read'     => '0'
            ))) {
                return array(
                    'status' => $status,
                    'count'  => 0
                );
            }
            return array(
                'status' => $status,
                'count'  => $data['nice_count'] + 1
            );
        }
        return array(
            'status' => $status,
            'count'  => 0
        );
    }

    /**
     * Get list Question Nice (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List question
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'user_name'),
            array('categories.name', 'category_name')
        )
            ->from(self::$_table_name)
            ->join('questions')
            ->on(self::$_table_name . '.question_id', '=', 'questions.id')
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('categories')
            ->on('questions.category_id', '=', 'categories.id');
        // filter by keyword
        if (!empty($param['question_id'])) {
            $query->where('question_id', '=', $param['question_id']);
        }
        if (!empty($param['user_id'])) {
            $query->where(self::$_table_name . '.user_id', '=', $param['user_id']);
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Disable a Question Nice
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $status = false;
        $options = array();
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        if (!empty($param['id'])) {
            $options['where'][] = array(
                'id' => $param['id'],
            );
        } else {
            $options['where'][] = array(
                'question_id' => $param['question_id'],
                'user_id'     => $param['user_id'],
                'disable'     => '0'
            );
        }
        $data = self::find('first', $options);
        if ($data) {
            $data->set('disable', $param['disable']);
            if ($data->update()) {
                $status = true;
            }
        }
        $count = \Model_Question::find($param['question_id'])['nice_count'];
        return array(
            'status' => $status,
            'count'  => !empty($count) ? $count : 0
        );
    }
}