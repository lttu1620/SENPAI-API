<?php

/**
 * Any query in Model Question
 *
 * @package Model
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Question extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'user_id',
        'category_id',
        'content',
        'to_univ',
        'to_high',
        'to_teacher',
        'answer_count',
        'nice_count',
        'favorite_count',
        'disable',
        'created',
        'updated',
        'status'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'questions';

    /**
     * Add info for Question
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Question id or false if error
     */
    public static function add($param)
    {
        $question = new self;
        // set value
        $question->set('user_id', $param['user_id']);
        $question->set('category_id', $param['category_id']);
        $question->set('content', $param['content']);
        $question->set('to_univ', $param['to_univ']);
        $question->set('to_high', $param['to_high']);
        $question->set('to_teacher', $param['to_teacher']);
        if (isset($param['status']))
            $question->set('status', $param['status']);
        else
            $question->set('status', 0);
        // add to database
        if ($question->create()) {
            $question->id = self::cached_object($question)->_original['id'];
            if (isset($param['medias'])) {
                \LogLib::error($param['medias']);
                $medias = json_decode($param['medias'], true);
                foreach ($medias as $media) {
                    $save_params = array(
                        'user_id' => $param['user_id'],
                        'question_id' => $question->id,
                        'media_url' => $media['url'],
                        'media_name' => $media['name'],
                        'type' => $media['type']
                    );
                    Model_Question_Media::add($save_params);
                }
            }
            return $question->id;
        }
        return false;
    }

    /**
     * Update info for Question
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function set_update($param)
    {
        // check exist
        $question = self::find($param['id']);
        if (empty($question)) {
            self::errorNotExist('question_id');
            return false;
        }
        // set value
        if (!empty($param['user_id'])) {
            $question->set('user_id', $param['user_id']);
        }
        if (!empty($param['category_id'])) {
            $question->set('category_id', $param['category_id']);
        }
        if (!empty($param['content'])) {
            $question->set('content', $param['content']);
        }
        if (isset($param['to_univ']) && $param['to_univ'] != '') {
            $question->set('to_univ', $param['to_univ']);
        }
        if (isset($param['to_high']) && $param['to_high'] != '') {
            $question->set('to_high', $param['to_high']);
        }
        if (isset($param['to_teacher']) && $param['to_teacher'] != '') {
            $question->set('to_teacher', $param['to_teacher']);
        }
        // save to database
        if ($question->save()) {
            return true;
        }
        return false;
    }

    /**
     * Get list Question (using array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List question
     */
    public static function get_list($param)
    {        
        if (empty($param['login_user_id'])) {
            $param['login_user_id'] = 0;
        }
        $query = DB::select(
                self::$_table_name . '.*',
                array('users.name', 'user_name'),
                array('users.sex_id', 'sex_id'),
                array('categories.name', 'category_name'),
                array('categories.color', 'color'),
                DB::expr("
                    IF(ISNULL(question_favorites.question_id),0,1) AS is_favorite
                "),
                DB::expr("
                    IF(ISNULL(question_nices.question_id),0,1) AS is_like
                "),
                DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url), IF(users.sex_id = 1, '" . \Config::get('no_image_user_male') . "', '".\Config::get('no_image_user_female')."')) AS user_image_url")
            )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('categories')
            ->on(self::$_table_name . '.category_id', '=', 'categories.id')
            ->join(DB::expr("
                (SELECT *
                FROM question_favorites
                WHERE disable = 0 AND user_id = {$param['login_user_id']}) question_favorites
            "), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'question_favorites.question_id')
            ->join(DB::expr("
                (SELECT *
                FROM question_nices
                WHERE disable = 0 AND user_id = {$param['login_user_id']}) question_nices
            "), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'question_nices.question_id');
        // filter by keyword
         if (!empty($param['joined_user_id'])) {
            $query->where( DB::expr("EXISTS (SELECT id FROM answers WHERE user_id = {$param['joined_user_id']} and questions.id = question_id AND disable = 0
                                        AND status = 1)"));
        } 
        
        if (!empty($param['report'])) {
            $query->where( DB::expr("EXISTS (SELECT id FROM violation_reports WHERE question_id = questions.id  AND disable = 0)"));
        } 
                
        if (!empty($param['category_id'])) {
            $query->where('category_id', '=', $param['category_id']);
        }
        if (!empty($param['user_id'])) {
            $query->where('users.id', '=', $param['user_id']);
        }
        if (!empty($param['user_number'])) {
            $query->where('users.number', '=', $param['user_number']);
        }
        if (!empty($param['content'])) {
            $query->where(self::$_table_name . '.content', 'LIKE', "%{$param['content']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (isset($param['status']) && $param['status'] != '') {
            $query->where(self::$_table_name . '.status', '=', $param['status']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        
        $medias = \Model_Question_Media::get_all_by_question(array('question_id' => \Lib\Arr::field($data, 'id')));
        foreach ($data as &$row) {
            $row['medias'] = !empty($medias[$row['id']]) ? ($medias[$row['id']]) : array();
        }
        if (isset($row)) {
            unset($row);
        }
        return array($total, $data);
    }

    /**
     * Get list Question for profile
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List question
     */
    public static function get_list_for_profile($param)
    {        
        $table = self::$_table_name;
        $query = DB::select(
                self::$_table_name . '.*',
                array('users.name', 'user_name'),
                array('users.sex_id', 'sex_id'),
                array('categories.name', 'category_name'),
                array('categories.color', 'color'),
                DB::expr("
                    IF(ISNULL(question_favorites.question_id),0,1) AS is_favorite
                "),
                DB::expr("
                    IF(ISNULL(question_nices.question_id),0,1) AS is_like
                "),
                array('users.image_url', 'user_image_url')
            )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('categories')
            ->on(self::$_table_name . '.category_id', '=', 'categories.id')
            ->join(DB::expr("
                (SELECT *
                FROM question_favorites
                WHERE disable = 0 AND user_id = {$param['user_id']}) question_favorites
            "), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'question_favorites.question_id')
            ->join(DB::expr("
                (SELECT *
                FROM question_nices
                WHERE disable = 0 AND user_id = {$param['user_id']}) question_nices
            "), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'question_nices.question_id')
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where(DB::expr("(
                {$table}.user_id = {$param['user_id']}
                OR 
                {$table}.id IN (SELECT question_id 
                                FROM answers 
                                WHERE disable = 0 AND user_id = {$param['user_id']}))"                
            ));             
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }     
        $data = $query->execute()->as_array();
        $medias = \Model_Question_Media::get_all_by_question(array('question_id' => \Lib\Arr::field($data, 'id')));
        foreach ($data as &$row) {
            $row['medias'] = !empty($medias[$row['id']]) ? ($medias[$row['id']]) : array();
        }
        if (isset($row)) {
            unset($row);
        }
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }
    
    /**
     * Get all Question (without array count)
     *
     * @author Le Tuan Tu
     * @param array $param Input array.
     * @return array List questions;
     */
    public static function get_all($param)
    {
        $param['login_user_id'] = !empty($param['login_user_id']) ? $param['login_user_id'] : 0;
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'user_name'),
            array('categories.name', 'category_name'),
            DB::expr("
                IF(ISNULL(question_favorites.question_id),0,1) AS is_favorite
            "),
            DB::expr("
                IF(ISNULL(question_nices.question_id),0,1) AS is_like
            ")
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('categories')
            ->on(self::$_table_name . '.category_id', '=', 'categories.id')
            ->join(DB::expr("
                (SELECT *
                FROM question_favorites
                WHERE disable = 0 AND user_id = {$param['login_user_id']}) question_favorites
            "), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'question_favorites.question_id')
            ->join(DB::expr("
                (SELECT *
                FROM question_nices
                WHERE disable = 0 AND user_id = {$param['login_user_id']}) question_nices
            "), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'question_nices.question_id')
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where('users.disable', '=', '0')
            ->where('categories.disable', '=', '0');
        if (isset($param['status']) && $param['status'] != '') {
            $query->where(self::$_table_name . '.status', '=', $param['status']);
        }
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Disable/Enable list Question
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $question = self::find($id);
            if ($question) {
                $question->set('disable', $param['disable']);
                if (!$question->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('question_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Question
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail question or false if error
     */
    public static function get_detail($param)
    {
        $param['login_user_id'] = !empty($param['login_user_id']) ? $param['login_user_id'] : 0;
        $query = DB::select(
            self::$_table_name . '.*',
            array('users.name', 'user_name'),
            array('categories.name', 'category_name'),
            array('categories.color', 'color'),
            array('users.sex_id', 'sex_id'),
            DB::expr("
                IF(ISNULL(question_favorites.question_id),0,1) AS is_favorite
            "),
            DB::expr("
                IF(ISNULL(question_nices.question_id),0,1) AS is_like
            "),
            DB::expr("IFNULL(IF(users.image_url='',NULL,users.image_url), IF(users.sex_id = 1, '" . \Config::get('no_image_user_male') . "', '".\Config::get('no_image_user_female')."')) AS user_image_url")
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('categories')
            ->on(self::$_table_name . '.category_id', '=', 'categories.id')
            ->join(DB::expr("
                (SELECT *
                FROM question_favorites
                WHERE disable = 0 AND user_id = {$param['login_user_id']}) question_favorites
            "), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'question_favorites.question_id')
            ->join(DB::expr("
                (SELECT *
                FROM question_nices
                WHERE disable = 0 AND user_id = {$param['login_user_id']}) question_nices
            "), 'LEFT')
            ->on(self::$_table_name . '.id', '=', 'question_nices.question_id')
            ->where(self::$_table_name . '.id', '=', $param['id']);
        // get data
        $data = $query->execute()->offsetGet(0);
        if (empty($data)) {
            static::errorNotExist('question_id');
            return false;
        } elseif (!empty($param['answer'])) {
            // Get list answers by question_id
            $options = array(
                'page' => !empty($param['page']) ? $param['page'] : 1,
                'limit' => !empty($param['limit']) ? $param['limit'] : 10000,
                'login_user_id' => $param['login_user_id'],
                'question_id' => $data['id'],                
                'disable' => 0,               
                'status' => 1,               
                'sort' => 'created-DESC',
            );
            if (isset($param['status'])) {
                $options['status'] = $param['status'];
            }
            $data['answers'] = Model_Answer::get_list(
                $options
            );
        }
        $data['medias'] = \Model_Question_Media::get_all(array('question_id' => $data['id']));
        return $data;
    }

    public static function get_count_answer($question_id)
    {
        $result = 0;
        $query = DB::select(
            self::$_table_name . '.answer_count'
        )
            ->from(self::$_table_name)
            ->where(self::$_table_name . '.id', '=', $question_id);
        $data = $query->execute()->offsetGet(0);
        if (empty($data['answer_count'])) $data['answer_count'] = 0;
        return $data['answer_count'];
    }

    /**
     * Get top n questions newest
     *
     * @author Lai Hac Thai
     * @param array $param Input data
     * @return array List question
     */
    public static function get_top($param)
    {
        $loginUserId = !empty($param['login_user_id']) ? $param['login_user_id'] : 0;
        $param = array(
            'page' => 1,
            'status' => isset($param['status']) ? $param['status'] : 1,
            'limit' => 20,
            'login_user_id' => $loginUserId,
            'disable' => 0,
            'sort' => 'created-DESC'
        );
        list($total, $data) = static::get_list($param);
        return $data;
    }

    /**
     * Update field answer_count, nice_count and favorite_count for Question
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function update_counter($param)
    {
        $idArray = explode(',', $param['question_id']);
        foreach ($idArray as $id) {
            $sql = "
                UPDATE questions
                SET    answer_count = (SELECT COUNT(*)
                                       FROM   answers
                                       WHERE  question_id = {$id}
                                              AND disable = 0),
                       nice_count = (SELECT COUNT(*)
                                     FROM   question_nices
                                     WHERE  question_id = {$id}
                                            AND disable = 0),
                       favorite_count = (SELECT COUNT(*)
                                         FROM   question_favorites
                                         WHERE  question_id = {$id}
                                                AND disable = 0)
                WHERE  id = {$id}
                       AND disable = 0
            ";
            DB::query($sql)->execute();
        }
        return true;
    }

    /**
     * Approve list Questions.
     *
     * @author truongnn
     * @param array $param Input array.
     * @return bool Success or otherwise.
     */
    public static function approve($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $question = self::find($id);
            if ($question) {
                $question->set('status', 1);
                if (!$question->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('question_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Reject list Questions.
     *
     * @author truongnn
     * @param array $param Input array.
     * @return bool Success or otherwise.
     */
    public static function reject($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $question = self::find($id);
            if ($question) {
                $question->set('status', 2);
                if (!$question->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('question_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get list Question for tadacopy
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List question
     */
    public static function get_list_for_tadacopy($param)
    {
        $query = DB::select(
            self::$_table_name . '.id',
            self::$_table_name . '.content',
            array(self::$_table_name . '.nice_count', 'nice'),
            array(self::$_table_name . '.favorite_count', 'favorite'),
            array('categories.name', 'category'), 
            self::$_table_name . '.created',
            self::$_table_name . '.disable',
            array('users.id', 'user_id'),
            array('users.name', 'user_name'),
            array('users.image_url', 'icon')
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('categories')
            ->on(self::$_table_name . '.category_id', '=', 'categories.id')
            ->where(self::$_table_name . '.status', '=', '1')
            ->where(self::$_table_name . '.to_univ', '=', '1')
            ->where(self::$_table_name . '.disable', '=', '0')
            ->where('users.disable', '=', '0')
            ->where('categories.disable', '=', '0');
        // filter by keyword
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        // get answers by list question id
        $answers = \Model_Answer::get_all_by_question(array(
            'question_id' => \Lib\Arr::field($data, 'id')
        ));
        foreach ($data as &$row) {
            $row['answers'] = !empty($answers[$row['id']]) ? ($answers[$row['id']]) : array();
        }
        return array($total, $data);
    }

    /**
     * Get detail Question for tadacopy
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail question or false if error
     */
    public static function get_detail_for_tadacopy($param)
    {
        $query = DB::select(
            self::$_table_name . '.id',
            self::$_table_name . '.content',
            array(self::$_table_name . '.nice_count', 'nice'),
            array(self::$_table_name . '.favorite_count', 'favorite'),
            array('categories.name', 'category'),            
            self::$_table_name . '.created',
            self::$_table_name . '.disable',
            array('users.id', 'user_id'),
            array('users.name', 'user_name'),
            array('users.image_url', 'icon')
        )
            ->from(self::$_table_name)
            ->join('users')
            ->on(self::$_table_name . '.user_id', '=', 'users.id')
            ->join('categories')
            ->on(self::$_table_name . '.category_id', '=', 'categories.id')
            ->where(self::$_table_name . '.id', '=', $param['question_id'])
            ->where(self::$_table_name . '.status', '=', '1')
            ->where(self::$_table_name . '.to_univ', '=', '1')
            ->where(self::$_table_name . '.disable', '=', '0');
        // get data
        $data = $query->execute()->offsetGet(0);
        if (empty($data)) {
            static::errorNotExist('question_id');
            return false;
        } else {
            $data['answers'] = Model_Answer::get_all_by_question(array(
                'question_id' => array($param['question_id'])
            ));
        }
        return $data;
    }
}
