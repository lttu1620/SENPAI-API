<?php

class Model_Answer_Media extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'user_id',
        'answer_id',
        'media_url',
        'media_name',
        'type',
        'created',
        'updated',
        'disable',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'answer_medias';

    /**
     * Get list Answer Image (using array count)
     *
     * @author caolp
     * @param array $param Input data
     * @return array List answer media
     */
    public static function get_all($param) {
        $where = array();
        if (isset($param['disable'])) {
            $where['disable'] = $param['disable'];
        }else{
            $where['disable'] = '0';
        }
        if (isset($param['answer_id'])) {
            $where['answer_id'] = $param['answer_id'];
        } if (isset($param['user_id'])) {
            $where['user_id'] = $param['user_id'];
        }
        $options['where'] = $where;
        $data = self::find('all', $options);
        return $data;
    }

    public static function get_all_by_answer($param) {
        if(count($param['answer_id']) == 0) return array();
        $query = DB::select()
            ->from(self::$_table_name) 
            ->where('disable', '=', '0');
        if (isset($param['answer_id'])) {          
             $query->where('answer_id', 'IN', $param['answer_id']);                       
        }  
        if (isset($param['type'])) {          
             $query->where('type', '=', $param['type']);                       
        }    
        $rows = $query->execute()->as_array();
        $result = array();
        if (!empty($rows)) {
            foreach ($rows as $row) {
                if (!isset($result[$row['answer_id']])) {
                    $result[$row['answer_id']] = array();
                }
                $result[$row['answer_id']][] = $row;
            }
        }
        return $result;
    }
    
    /**
     * Get detail Answer Image
     *
     * @author caolp
     * @param array $param Input data
     * @return array|bool Detail answer media or false if error
     */
    public static function get_detail($param) {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('media_id');
            return false;
        }
        return $data;
    }

    /**
     * Add info for Answer Image
     *
     * @author caolp
     * @param array $param Input data.
     * @return int|bool Answer Image id or false if error
     *
     */
    public static function add($param) {
        $query = DB::select(
                        array('answers.id', 'answer_id'), 
                        array('answer_medias.id', 'media_id'), 
                        array('answer_medias.media_url', 'media_url'), 
                        array('answer_medias.user_id', 'user_id'), 
                        array('answer_medias.disable', 'media_disable')
                )
                ->from('answers')
                ->join(
                        DB::expr(
                                "(SELECT * FROM answer_medias
                                  WHERE user_id = {$param['user_id']}) AS answer_medias"
                        ), 'LEFT'
                )
                ->on('answers.id', '=', 'answer_medias.answer_id')
                ->where('answers.id', '=', $param['answer_id'])
                ->where('answers.disable', '=', '0');
        $data = $query->execute()->offsetGet(0);

        if (empty($data['answer_id'])) {
            static::errorNotExist('answer_id');
            return false;
        }
        if (!empty($data['user_id']) && $data['media_url'] == $param['media_url'] && $data['media_disable'] == 0) {
            static::errorDuplicate('media_url');
            return false;
        }
        $new = false;
        if (!empty($data['user_id']) && $data['media_url'] == $param['media_url'] && $data['media_disable'] == 1) {
            $dataUpdate = array(
                'id' => $data['media_id'],
                'media_url' => $param['media_url'],
                'media_name' => $param['media_name'],
                'type' => $param['type'],
                'disable' => '0'
            );
        } else {
            $new = true;
            $dataUpdate = array(
                'answer_id' => $data['answer_id'],
                'user_id' => $param['user_id'],
                'media_url' => $param['media_url'],
                'media_name' => $param['media_name'],
                'type' => $param['type'],
            );
        }
        $Image = new self($dataUpdate, $new);
        if ($Image->save()) {
            if ($new == true) {
                $Image->id = self::cached_object($Image)->_original['id'];
            }
            return $Image->id;
        }
        return false;
    }

    /**
     * Disable a Answer Image
     *
     * @author caolp
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param) {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $item = self::find($id);
            if ($item) {
                $item->set('disable', $param['disable']);
                if (!$item->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('media_id');
                return false;
            }
        }
        return true;
    }

}
