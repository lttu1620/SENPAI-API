<?php
/**
 * Model to operate to Answer_Nice's functions.
 *
 * @package Model
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Model_Answer_Nice extends Model_Abstract
{
	protected static $_properties = array(
		
		'id',
		'user_id',
		'answer_id',
		'created',
		'updated',
		'disable',
	);

	protected static $_observers = array(
		'Orm\Observer_CreatedAt' => array(
			'events' => array('before_insert'),
			'mysql_timestamp' => false,
		),
		'Orm\Observer_UpdatedAt' => array(
			'events' => array('before_update'),
			'mysql_timestamp' => false,
		),
	);

	protected static $_table_name = 'answer_nices';
        
     /**
     * Get list answer_nices.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return array Returns array(total, data).
     *
     */
    public static function get_list($param)
    {
        $query = DB::select(
            self::$_table_name . ".*",
            array('users.name', 'user_name'),
            array('users.mail', 'user_mail'),
            array('answers.content', 'answer_content')
        )
            ->from(self::$_table_name)
            ->join('answers', 'LEFT')
            ->on(self::$_table_name . ".answer_id", '=', 'answers.id')
            ->join('users', 'LEFT')
            ->on(self::$_table_name . ".user_id", '=', 'users.id');

        if (!empty($param['user_id'])) {
            $query->where('users.id', $param['user_id']);
        }
        if (!empty($param['user_name'])) {
            $query->where('users.name', 'LIKE', "%{$param['user_name']}%");
        }
        if (!empty($param['answer_id'])) {
            $query->where('answers.id', $param['answer_id']);
        }

        if (!empty($param['mail'])) {
            $query->where('users.mail', '=', "{$param['mail']}");
        }
        if (!empty($param['answer_id'])) {
            $query->where('answers.id', '=', $param['answer_id']);
        }
       
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Disable a or any answer_nices.
     *
     * @author truongnn
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool Returns the boolean.
     *
     */
    public static function disable($param)
    {        
            $options = array();
        if (!isset($param['disable'])) {
            $param['disable'] = '1';
        }
        if (!empty($param['id'])) {
            $options['where'][] = array(
                'id' => $param['id'],
            );
        } else {
            $options['where'][] = array(
                'answer_id' => $param['answer_id'],
                'user_id' => $param['user_id'],
                'disable' => '0'
            );
        }
        $status = false;
        $data = self::find('first', $options);
        if ($data) {
            $data->set('disable', $param['disable']);
            if ($data->update()) {
                $status = true;
            }
        }
         $count = \Model_Answer::find($param['answer_id'])['nice_count'];
        return array(
            'status' => $status,
            'count' => !empty($count) ? $count : 0
        );
    }

    /**
     * Add info for answer_nices.
     *
     * @author truongnn
     * @param array $param Input data.
     * @return int|bool Return the integer or boolean.
     *
     */
    public static function add($param)
    {
        $query = DB::select(
            array('answers.id', 'answer_id'),
            array('answers.nice_count', 'nice_count'),
            array('answer_nices.id', 'answer_nice_id'),
            array('answer_nices.user_id', 'user_id'),
            array('answer_nices.disable', 'answer_nice_disable')
        )
            ->from('answers')
            ->join('questions', 'LEFT')
            ->on('answers.question_id', '=', 'questions.id')
            ->join(
                DB::expr(
                    "(SELECT * FROM answer_nices WHERE user_id={$param['user_id']}) AS answer_nices"
                ),
                'LEFT'
            )
            ->on('answers.id', '=', 'answer_nices.answer_id')
            ->where('answers.id', '=', $param['answer_id'])
            ->where('answers.disable', '=', '0');
        $data = $query->execute()->offsetGet(0);
        $status = false;
        if (empty($data['answer_id'])) {
            return array(
                'status' => $status,
                'count'  => 0
            );
        }
        if (!empty($data['user_id']) && $data['answer_nice_disable'] == 0) {
            //static::errorDuplicate('user_id', $param['user_id']);
            return array(
                'status' => $status,
                'count'  => $data['nice_count']
            );
        }
        $new = false;
        if (!empty($data['user_id']) && $data['answer_nice_disable'] == 1) {
            $status = true;
            $dataUpdate = array(
                'id'      => $data['answer_nice_id'],
                'disable' => '0'
            );
        } else {
            $new = true;
            $status = true;
            $dataUpdate = array(
                'answer_id' => $param['answer_id'],
                'user_id'   => $param['user_id']
            );
        }
        $failure = false;
        $nices = new self($dataUpdate, $new);
        if ($nices->save()) {
            // notice for user that created this answer
            $answer = Model_Answer::get_detail(array(
                'id' => $param['answer_id']
            ));
            if ($answer) {
                if (!Model_Notice::add(array(
                    'question_id' => $answer['question_id'],
                    'user_id'     => $param['user_id'],
                    'type'        => Config::get('type_notice')['Like_MyAnswerQuestion'],
                    'answer_id'   => $param['answer_id'],
                    'is_read'     => '0'
                ))) {
                    $failure = true;
                }
            } else {
                $failure = true;
            }
        }
        if (!$failure) {
            //have trigger to update like_count for this comment.
            return array(
                'status' => $status,
                'count'  => $data['nice_count'] + 1
            );
        } else {
            return array(
                'status' => $status,
                'count'  => 0
            );
        }
    }
}
