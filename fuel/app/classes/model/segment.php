<?php

/**
 * Any query in Model Segment
 *
 * @package Model
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Segment extends Model_Abstract
{
    /** @var array $_properties field of table */
    protected static $_properties = array(
        'id',
        'name',
        'disable',
        'created',
        'updated',
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    /** @var array $_table_name name of table */
    protected static $_table_name = 'segments';

    /**
     * Add or update info for Segment
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return int|bool Segment id or false if error
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $segment = new self;
        // check exist
        if (!empty($id)) {
            $segment = self::find($id);
            if (empty($segment)) {
                self::errorNotExist('segment_id');
                return false;
            }
        }
        // set value
        if (!empty($param['name'])) {
            $segment->set('name', $param['name']);
        }
        // save to database
        if ($segment->save()) {
            if (empty($segment->id)) {
                $segment->id = self::cached_object($segment)->_original['id'];
            }
            return !empty($segment->id) ? $segment->id : 0;
        }
        return false;
    }

    /**
     * Get list Segment (using for admin page)
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array List segment
     */
    public static function get_list($param)
    {
        $query = DB::select()->from(self::$_table_name);
        // filter by keyword
        if (!empty($param['name'])) {
            $query->where('name', 'LIKE', "%{$param['name']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by(self::$_table_name . '.' . $sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        // get data
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;
        return array($total, $data);
    }

    /**
     * Get all Segment (using for mobile)
     *
     * @author Le Tuan Tu
     * @return array List segment
     */
    public static function get_all()
    {
        $query = DB::select()
            ->from(self::$_table_name)
            ->where('disable', '=', '0');
        // get data
        $data = $query->execute()->as_array();
        return $data;
    }

    /**
     * Enable/disable list Segment
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return bool Success or otherwise
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $segment = self::find($id);
            if ($segment) {
                $segment->set('disable', $param['disable']);
                if (!$segment->save()) {
                    return false;
                }
            } else {
                self::errorNotExist('segment_id');
                return false;
            }
        }
        return true;
    }

    /**
     * Get detail Segment
     *
     * @author Le Tuan Tu
     * @param array $param Input data
     * @return array|bool Detail segment or false if error
     */
    public static function get_detail($param)
    {
        $data = self::find($param['id']);
        if (empty($data)) {
            static::errorNotExist('segment_id');
            return false;
        }
        return $data;
    }
}
