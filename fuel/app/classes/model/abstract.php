<?php

use Orm\Model;
use Lib\Cache;

/**
 * Model_Abstract - Model to create common functions or constants
 *
 * @package Model
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Model_Abstract extends Model {

    public $disable = 0;     
    public static $error_code_validation = array();
    const ERROR_CODE_INVALED_PARAMETER = 400;
    const ERROR_CODE_AUTH_ERROR = 403;
    const ERROR_CODE_FIELD_NOT_EXIST = 1010;
    const ERROR_CODE_FIELD_DUPLICATE = 1011;
    const ERROR_CODE_OTHER_1 = 1021;
    const ERROR_CODE_OTHER_2 = 1022;
    const ERROR_CODE_OTHER_3 = 1023;
    const ERROR_CODE_OTHER_4 = 1024;
    const ERROR_CODE_OTHER_5 = 1025;

    /**
     * <init - function to inital properties>
     *
     * @author thailh
     */
    public static function _init() 
    {
        if (\Lib\Util::os() != \Config::get('os')['webos'] && !empty(static::$_mobile_properties)) 
        {
            static::$_properties = static::$_mobile_properties;
        }
    }

    /**
     * <errorParamInvalid - function to set value for error_code cause INVALED_PARAMETER>
     *
     * @author thailh
     */
    public static function errorParamInvalid($field = '', $value = '') 
    {
        static::$error_code_validation[] = array(
            'code' => self::ERROR_CODE_INVALED_PARAMETER,
            'field' => $field,
            'value' => $value,
        );
    }

    /**
     * <errorNotExist - function to set value for error_code cause FIELD_NOT_EXIST>
     *
     * @author thailh
     */
    public static function errorNotExist($field, $value = '') 
    {
        static::$error_code_validation[] = array(
            'code' => self::ERROR_CODE_FIELD_NOT_EXIST,
            'field' => $field,
            'value' => $value,
        );
    }

    /**
     * <errorDuplicate - function to set value for error_code cause FIELD_DUPLICATE>
     *
     * @author thailh
     */
    public static function errorDuplicate($field, $value = '')
    {
        static::$error_code_validation[] = array(
            'code' => self::ERROR_CODE_FIELD_DUPLICATE,
            'field' => $field,
            'value' => $value,
        );
    }

    /**
     * <errorOther - function to set value for error_code cause others>
     *
     * @author thailh
     */
    public static function errorOther($code, $field = null, $value = '') 
    {
        static::$error_code_validation[] = array(
            'code' => $code,
            'field' => $field,
            'value' => $value,
        );
    }

    /**
     * <error - function to set value for error_code_validation>
     *
     * @author thailh
     * @return array
     */
    public static function error() {
        return static::$error_code_validation;
    }

    /**
     * <date_from_val - function to format date>
     *
     * @author thailh
     * @return int
     */
    public static function date_from_val($date) {
        return strtotime($date);
    }

    /**
     * <date_to_val - function to format date time>
     *
     * @author thailh
     * @return int
     */
    public static function date_to_val($date) {
        return strtotime($date . '23:59:00');
    }
    
    /**
     * batchInsert
     *
     * @param string $table Table name
     * @param array $data Data for insert/update
     * @param array $updates Data for update if duplicate keys
     * @param boolean $ignore Ignore duplicate or not
     * @author thailh
     * @return boolean True if success otherwise false
     */
    public static function batchInsert($table, $data, $updates = array(), $ignore = true) {
        if (empty($data)) {
            return false;
        }
        if (empty($data[0])) {
            $data = array($data);
        }
        if (!empty($ignore)) {
            $ignore = 'IGNORE';
        }
        $inserts = $field = array();
        $data = DB::quote($data);
        foreach ($data as $i => $row) {
            $insert = array();
            foreach ($row as $key => $val) {
                if ($i == 0) {
                    $field[] = $key;
                }                
                $insert[] = $val;
            }
            $inserts[] = "(" . implode(',', $insert) . ")";
        }
        if (!empty($inserts)) {            
            $sql = " INSERT {$ignore} INTO {$table}(" . implode(",", $field) . ")";
            $sql .= " VALUES " . implode(",", $inserts);
            if (!empty($updates)) { 
                $updates = DB::quote($updates);
                $updateSQL = array();                
                foreach ($updates as $field => $value) {                   
                    $updateSQL[] = "{$field}={$value}";
                }
                $sql .= " ON DUPLICATE KEY UPDATE " . implode(",", $updateSQL);
            }
            return DB::query($sql)->execute();
        }
        return false;
    }

}
