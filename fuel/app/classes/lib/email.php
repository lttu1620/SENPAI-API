<?php
/**
 * Support functions for Email
 *
 * @package Lib
 * @created 2014-11-25
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
namespace Lib;

use Fuel\Core\Config;

class Email {

    /**
     * Send register email   
     *  
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendRegisterEmail($param) {       
        $to = $param['email'];
//        $email = \Email::forge('jis','aws_ses');
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Capture No reply');
        $email->subject('Capture 仮登録');
        $data['url']= Config::get('adm_url') . "pages/active/{$param['token']}";

        $body = \View::forge('email/pc/register', $data);
        //$email->body(mb_convert_encoding($body, 'jis'));
        $email->html_body($body);

        $email->to($to);
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            return $email->send();
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        }
    }

    /**
     * Send approve email   
     *  
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendApproveEmail($param) 
    {
        if (empty($param['token']) && !empty($param['user_id']) && !empty($param['email'])) 
        {            
            $param['token'] = \Lib\Str::generate_token();
            $userActivation = new \Model_User_Activation();
            $userActivation->set('user_id', $param['user_id']);
            $userActivation->set('email', $param['email']);
            $userActivation->set('token', $param['token']);          
            $userActivation->set('expire_date', \Config::get('register_token_expire'));
            $userActivation->set('regist_type', \Config::get('user_activations_type')['register_company']);
            if (!$userActivation->save()) 
            {
                return false;
            }  
        }
        if (empty($param['token']) || empty($param['email']))
        {
            \LogLib::warning('Parameter is invalid', __METHOD__, $param);
            return false;
        }
        $to = $param['email'];
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'No reply');
        $email->subject('Capture 承認申請');

        $data['url']= Config::get('adm_url') . "pages/approve/{$param['token']}";
        $body = \View::forge('email/pc/approve', $data);
         //$email->body(mb_convert_encoding($body, 'jis'));
        $email->html_body($body);
        $email->to($to);
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            return $email->send();
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        }
    }

    /**
     * Execute to send email after added contact successfully
     *
     * @author truongnn
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendContactEmail($param) {
        $emailConfig = Config::get('system_email');
        $to = $param['email'];
        $email = \Email::forge();
        $email->from(Config::get('system_email.noreply'), 'Capture No reply');
        $email->subject($param['subject']);
        $email->body($param['content']);
        $email->to($to);
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            return $email->send();            
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        }
    }

    /**
     * Send email if forgetting password  
     *  
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendForgetPasswordEmail($param) {
        $to = $param['email'];
        $subject = 'Capture パスワードの再設定';
    
        $emailConfig = Config::get('system_email');
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Capture No reply');
        $email->subject("$subject");

        $data['url']= Config::get('adm_url') . "pages/newpassword/{$param['token']}";
        $body = \View::forge('email/pc/forget_password', $data);
        //$email->body(mb_convert_encoding($body, 'jis'));
        $email->html_body($body);
        
        $email->to($to);
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            return $email->send();            
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        }
    }

    /**
     * Send email if forgetting password for mobile only   
     *  
     * @author thailh
     * @param array $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendForgetPasswordEmailForMobile($param) {
        $to = $param['email'];
        $subject = 'Capture パスワードの再設定';

        $emailConfig = Config::get('system_email');
        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Capture No reply');
        $email->subject("$subject");

        $data['keycode']= $param['token'];
        $body = \View::forge('email/mobile/forget_password', $data);
        //$email->body(mb_convert_encoding($body, 'jis'));
        $email->body($body);


        $email->to($to);
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            return $email->send();
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        }
    }

    /**
     * send mail to user's email when user be activated
     *
     * @author Le Tuan Tu
     * @param $param Information for sending email
     * @return bool Return true if successful ortherwise return false
     */
    public static function sendRegisterActiveEmail($param){
        $to = $param['email'];
        $subject = 'Capture 登録完了';

        $email = \Email::forge('jis');
        $email->from(Config::get('system_email.noreply'), 'Capture No reply');
        $email->subject("$subject");
        if (isset($param['approved_flg'])) {
            // $email->body('Your request has been approved');
            $data['active_name'] = "あなたのリクエストが承認されました。";
        } else {
            // $email->body('Your registration is activated!');
            $data['active_name'] = "メールアドレスが確認されました。";
        }
        $body = \View::forge('email/register_active', $data);
         //$email->body(mb_convert_encoding($body, 'jis'));
        $email->body($body);

        $email->to($to);
        try {
            \LogLib::info("Sent email to {$to}", __METHOD__, $param);
            return $email->send();
        } catch (\EmailSendingFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        } catch (\EmailValidationFailedException $e) {
            \LogLib::warning($e, __METHOD__, $param);
            return false;
        }
    }

}
