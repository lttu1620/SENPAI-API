<?php

/**
 * Controller for actions to report
 *
 * @package Controller
 * @created 2014-12-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Reports extends \Controller_App
{
    /**
     *  Get list commont report data
     * 
     * @return boolean 
     */
	public function action_general() {
		return \Bus\Reports_General::getInstance()->execute();
	}

    /**
     *  Get list newsFeedShares report data
     * 
     * @return boolean 
     */
	public function action_newsFeedShares() {
		return \Bus\Reports_NewsFeedShares::getInstance()->execute();
	}
    /**
     * Get list login report data
     * 
     * @return boolean 
     */
	public function action_login() {
		return \Bus\Reports_Login::getInstance()->execute();
	}

    /**
     *  Get list newscommentLike report data
     * 
     * @return boolean 
     */
	public function action_newsCommentLikes() {
		return \Bus\Reports_NewsCommentLikes::getInstance()->execute();
	}

    /**
     *  Get list newsFeedReads report data
     * 
     * @return boolean 
     */
	public function action_newsFeedReads() {
		return \Bus\Reports_NewsFeedReads::getInstance()->execute();
	}

    /**
     *  Get list newsFeedViewport data
     * 
     * @return boolean 
     */
	public function action_newsFeedViews() {
		return \Bus\Reports_NewsFeedViews::getInstance()->execute();
	}

	/**
     *  Get list newsFeedlikes sport data
     * 
     * @return boolean 
     */
	public function action_newsFeedLikes()
	{
		return \Bus\Reports_NewsFeedLikes::getInstance()->execute();
	}

	/**
     *  Get list newsFeedFavorites report data
     * 
     * @return boolean 
     */
	public function action_newsFeedFavorites()
	{
		return \Bus\Reports_NewsFeedFavorites::getInstance()->execute();
	}
    
    /**
     *  Get list newsFeedpv report data
     * 
     * @return boolean 
     */
    public function action_newsfeedspv() {
		return \Bus\Reports_NewsFeedsPv::getInstance()->execute();
	}
    
    /**
     *  Get list newsFeedsuu report data
     * 
     * @return boolean 
     */
    public function action_newsfeedsuu() {
		return \Bus\Reports_NewsFeedsUu::getInstance()->execute();
	}
}