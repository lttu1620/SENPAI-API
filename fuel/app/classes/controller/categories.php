<?php

/**
 * Controller for actions on Category
 *
 * @package Controller
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Categories extends \Controller_App
{
    /**
     * Add or update info for Category
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Categories_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Category (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Categories_List::getInstance()->execute();
    }

    /**
     * Get all Category (without array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Categories_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Category
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Categories_Disable::getInstance()->execute();
    }

    /**
     * Get detail Category
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Categories_Detail::getInstance()->execute();
    }

    /**
     * Update field question_count for Category
     *
     * @return boolean
     */
    public function action_updateCounter()
    {
        return \Bus\Categories_UpdateCounter::getInstance()->execute();
    }
}
