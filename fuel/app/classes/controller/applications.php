<?php

/**
 * Controller_Applications.
 *
 * @package Controller
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Controller_Applications extends \Controller_App
{
    /**
     * Get detail for Application.
     *  
     * @return boolean
     */
    public function action_detail() {
        return \Bus\Applications_Detail::getInstance()->execute();
    }

    /**
     * Get list for Application.  
     *  
     * @return boolean
     */
    public function action_list() {
        return \Bus\Applications_List::getInstance()->execute();
    }

    /**
     * Update disable field for Application.  
     *  
     * @return boolean
     */
    public function action_disable() {
        return \Bus\Applications_Disable::getInstance()->execute();
    }

    /**
     * Update or add new informatiob for Application.
     *  
     * @return boolean
     */
    public function action_addupdate() {
        return \Bus\Applications_AddUpdate::getInstance()->execute();
    }

    /**
     * Get all for Application.  
     *  
     * @return boolean
     */
    public function action_all() {
        return \Bus\Applications_All::getInstance()->execute();
    }
    
     /**
     * Get list for Application.  
     *  
     * @return boolean
     */
    public function action_listbyuserid() {
        return \Bus\Applications_ListbyUserId::getInstance()->execute();
    }
}