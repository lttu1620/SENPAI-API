<?php

/**
 * Controller for actions on User (in frontend).
 *
 * @package Controller
 * @created 2014-12-19
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Controller_Fe_Users extends \Controller_App
{
    /**
     * Action detail.   
     *  
     * @author truongnn
     * @return void 
     */
	public function action_detail()
	{
		return \Bus\Fe_Users_Detail::getInstance()->execute();
	}

}
