<?php

/**
 * Controller_Fe_UserSettings- Controller for actions on userSetting on mobile.
 *
 * @package Controller
 * @created 2014-12-19
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Controller_Fe_Usersettings extends \Controller_App {

    /**
     * Action post_list.   
     *  
     * @author tuancd
     * @return void 
     */
    public function post_list() {
        return \Bus\Fe_Usersettings_List::getInstance()->execute();
    }

    /**
     * Action get_list.   
     *  
     * @author tuancd
     * @return void 
     */
    public function get_list() {
        return $this->post_list();
    }

    /**
     * Action post_update.   
     *  
     * @author tuancd 
     * @return void
     */
    public function post_update() {
        return \Bus\Fe_Usersettings_Update::getInstance()->execute();
    }

    /**
     * Action get_update .  
     *  
     * @author tuancd 
     * @return void
     */
    public function get_update() {
        return $this->post_update();
    }

}
