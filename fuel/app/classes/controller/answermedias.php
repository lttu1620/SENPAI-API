<?php

/**
 * Controller for actions on Answer Media
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
class Controller_AnswerMedias extends \Controller_App
{
    
    
    /**
     * Get all Answer Media (using array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\AnswerMedias_All::getInstance()->execute();
    }
    
    
    /**
     * Get detail Answer Media (using array count)
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\AnswerMedias_Detail::getInstance()->execute();
    }
    
    /**
     * Add info for Answer Media
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\AnswerMedias_Add::getInstance()->execute();
    }


    /**
     * Disable/Enable a Answer Media
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\AnswerMedias_Disable::getInstance()->execute();
    }
}
