<?php

/**
 * Controller_Enrollments.
 *
 * @package Controller
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Controller_Enrollments extends \Controller_App
{
    /**
     * Get detail for Enrollment.
     *  
     * @return boolean
     */
    public function action_detail() {
        return \Bus\Enrollments_Detail::getInstance()->execute();
    }

    /**
     * Get list for Enrollment.  
     *  
     * @return boolean
     */
    public function action_list() {
        return \Bus\Enrollments_List::getInstance()->execute();
    }

    /**
     * Update disable field for Enrollment.  
     *  
     * @return boolean
     */
    public function action_disable() {
        return \Bus\Enrollments_Disable::getInstance()->execute();
    }

    /**
     * Update or add new informatiob for Enrollment.
     *  
     * @return boolean
     */
    public function action_addupdate() {
        return \Bus\Enrollments_AddUpdate::getInstance()->execute();
    }

    /**
     * Get all for Enrollment.  
     *  
     * @return boolean
     */
    public function action_all() {
        return \Bus\Enrollments_All::getInstance()->execute();
    }
}