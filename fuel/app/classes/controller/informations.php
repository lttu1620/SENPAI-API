<?php

/**
 * Controller_Informations.
 *
 * @package Controller
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Controller_Informations extends \Controller_App
{
    /**
     * Get detail for Information.
     *  
     * @return boolean
     */
    public function action_detail() {
        return \Bus\Informations_Detail::getInstance()->execute();
    }

    /**
     * Get list for Information.  
     *  
     * @return boolean
     */
    public function action_list() {
        return \Bus\Informations_List::getInstance()->execute();
    }

    /**
     * Update disable field for Information.  
     *  
     * @return boolean
     */
    public function action_disable() {
        return \Bus\Informations_Disable::getInstance()->execute();
    }

    /**
     * Update or add new informatiob for Information.
     *  
     * @return boolean
     */
    public function action_addupdate() {
        return \Bus\Informations_AddUpdate::getInstance()->execute();
    }

    /**
     * Get all for Information.  
     *  
     * @return boolean
     */
    public function action_all() {
        return \Bus\Informations_All::getInstance()->execute();
    }
    
     /**
     * Update print field for Information.  
     *  
     * @return boolean
     */
    public function action_printed() {
        return \Bus\Informations_Printed::getInstance()->execute();
    }
}