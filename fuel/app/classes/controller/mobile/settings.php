<?php

/**
 * Controller_Mobile_UserSettings- Controller for actions on userSetting on mobile
 *
 * @package Controller
 * @created 2014-12-19
 * @version 1.0
 * @author tuancd
 * @copyright Oceanize INC
 */
class Controller_Mobile_Settings extends \Controller_App {

    /**
     * Action global   
     *  
     * @author tuancd 
     * @return void
     */
    public function action_global() {
        return \Bus\Mobile_Settings_Global::getInstance()->execute();
    }

}
