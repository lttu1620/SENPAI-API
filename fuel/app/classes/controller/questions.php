<?php

/**
 * Controller for actions on Question
 *
 * @package Controller
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Questions extends \Controller_App
{
    /**
     * Add info for Question
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\Questions_Add::getInstance()->execute();
    }

    /**
     * Update info for Question
     *
     * @return boolean
     */
    public function action_update()
    {
        return \Bus\Questions_Update::getInstance()->execute();
    }


    /**
     * Get list Question (using for admin page)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Questions_List::getInstance()->execute();
    }

    /**
     * Get all Question (using for mobile)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Questions_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Question
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Questions_Disable::getInstance()->execute();
    }

    /**
     * Get detail Question
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Questions_Detail::getInstance()->execute();
    }

    /**
     * Get top n questions newest
     *
     * @return boolean
     */
    public function action_top()
    {
        return \Bus\Questions_Top::getInstance()->execute();
    }

    /**
     * Update field answer_count, nice_count and favorite_count for Question
     *
     * @return boolean
     */
    public function action_updateCounter()
    {
        return \Bus\Questions_UpdateCounter::getInstance()->execute();
    }
    /**
     * Approve list Questions.
     *
     * @return boolean
     */
    public function action_approve()
    {
        return \Bus\Questions_Approve::getInstance()->execute();
    }
    /**
     * Reject list Questions.
     *
     * @return boolean
     */
    public function action_reject()
    {
        return \Bus\Questions_Reject::getInstance()->execute();
    }
}
