<?php

/**
 * Controller for actions on Answer
 *
 * @package Controller
 * @created 2015-04-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Tadacopy_Answers extends \Controller_App
{
    /**
     * Add info for Answer
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\Tadacopy_Answers_Add::getInstance()->execute();
    }
}
