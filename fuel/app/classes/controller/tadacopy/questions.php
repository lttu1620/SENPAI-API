<?php

/**
 * Controller for actions on Question
 *
 * @package Controller
 * @created 2015-04-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Tadacopy_Questions extends \Controller_App
{
    /**
     * Get list Question
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Tadacopy_Questions_List::getInstance()->execute();
    }

    /**
     * Get detail Question
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Tadacopy_Questions_Detail::getInstance()->execute();
    }
}
