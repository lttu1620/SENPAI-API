<?php

/**
 * Controller for actions on admin
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Public extends \Controller_App {

    /**
     *  Action after login by facebook
     * 
     * @return object 
     */
    public function action_fblogin() {
        @session_start();
        FacebookSession::setDefaultApplication(\Config::get('facebook.app_id'), \Config::get('facebook.app_secret'));   
        $helper = new FacebookRedirectLoginHelper(\Uri::current()); 
        try {
            $session = $helper->getSessionFromRedirect(); 
            if (!isset($session)) {
                return Response::redirect($helper->getLoginUrl(array('scope' => 'email')));          
            } else {
                $param['token'] = $session->getToken();
                $info = \Model_User::login_facebook_by_token($param, 1);      
                p($info, 1);
            }           
        } catch(FacebookRequestException $ex) {
            p($ex);
        } catch(\Exception $ex) {
            p($ex);
        }       
    }
}
