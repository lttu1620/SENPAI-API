<?php

/**
 * Controller_Images
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Controller_Images extends \Controller_Rest {

    /**
     *  Crop image  
     * 
     * @return boolean
     */
    public function action_crop() {       
        return \Bus\Images_Crop::getInstance()->execute();       
    }

}
