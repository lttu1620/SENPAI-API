<?php

/**
 * Controller_Users - Controller for actions on Users
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_Users extends \Controller_App {

    /**
     *  Get list user by condition
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\Users_List::getInstance()->execute();
    }

    /**
     *  Get detail of user
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\Users_Detail::getInstance()->execute();
    }

   /**
     *  Update disable field for user
    * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\Users_Disable::getInstance()->execute();
    }

    /**
     *  Update or add new user
     * 
     * @return boolean 
     */
    public function action_addupdate() {
        return \Bus\Users_AddUpdate::getInstance()->execute();
    }

    /**
     *  Register user by email
     * 
     * @return boolean 
     */
    public function action_registerEmail() {
        return \Bus\Users_RegisterEmail::getInstance()->execute();
    }

    /**
     *  Resend email for user has register requested
     * 
     * @return boolean 
     */
    public function action_resendRegisterEmail() {
        return \Bus\Users_ResendRegisterEmail::getInstance()->execute();
    }

    /**
     *  Action forget password for user
     * 
     * @return boolean 
     */
    public function action_forgetpassword() {
        return \Bus\Users_Forgetpassword::getInstance()->execute();
    }

    /**
     *  Actice for user has register requested
     * 
     * @return boolean 
     */
    public function action_registerActive() {
        return \Bus\Users_RegisterActive::getInstance()->execute();
    }
    
    /**
     *  Login for user
     * 
     * @return boolean 
     */
    public function action_login() {
        return \Bus\Users_Login::getInstance()->execute();
    }
    /**
     *  Login for user
     *
     * @return boolean
     */
    public function action_loginbynumber() {

        return \Bus\Users_LoginByNumber::getInstance()->execute();
    }

    /**
     *  Login facebook for user
     * 
     * @return boolean 
     */
    public function action_fblogin() {
        return \Bus\Users_FbLogin::getInstance()->execute();
    }    
    
    /**
     *  Login facebook by token for user
     * 
     * @return boolean 
     */
    public function action_fblogintoken() {
        return \Bus\Users_FbLoginToken::getInstance()->execute();
    }  
    
    /**
     *  Register company for user
     * 
     * @return boolean 
     */
    public function action_registercompany() {
        return \Bus\Users_RegisterCompany::getInstance()->execute();
    }

    /**
     * Resend register company for user
     * 
     * @return boolean 
     */
    public function action_resendRegisterCompany() {
        return \Bus\Users_ResendRegisterCompany::getInstance()->execute();
    }

    /**
     *  Get token of user
     * 
     * @return boolean 
     */
    public function action_token() {
        return \Bus\Users_Token::getInstance()->execute();
    }

    /**
     *  Guest login action
     * 
     * @return boolean 
     */
    public function action_guestLogin() {
        return \Bus\Users_GuestLogin::getInstance()->execute();
    }
    
    /**
     * Resend email for user forget password
     * 
     * @return boolean 
     */
    public function action_resendForgetPassword() {
        return \Bus\Users_ResendForgetPassword::getInstance()->execute();
    }
    
    /**
     *  Statistic by user_id.
     * 
     * @return boolean 
     */
    public function action_statistic() {
        return \Bus\Users_Statistic::getInstance()->execute();
    }
    
    /**
     *  Get profile of user.
     * 
     * @return boolean 
     */
    public function action_profile() {
        return \Bus\Users_Profile::getInstance()->execute();
    }
}
