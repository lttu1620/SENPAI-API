<?php

/**
 * Controller for actions on Segment
 *
 * @package Controller
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Segments extends \Controller_App
{
    /**
     * Add or update info for Segment
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\Segments_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list Segment (using for admin page)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Segments_List::getInstance()->execute();
    }

    /**
     * Get all Segment (using for mobile)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Segments_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Segment
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Segments_Disable::getInstance()->execute();
    }

    /**
     * Get detail Segment
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Segments_Detail::getInstance()->execute();
    }
}
