<?php

/**
 * Controller for actions on Answer
 *
 * @package Controller
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Answers extends \Controller_App
{
    /**
     * Add info for Answer
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\Answers_Add::getInstance()->execute();
    }

    /**
     * Update info for Answer
     *
     * @return boolean
     */
    public function action_update()
    {
        return \Bus\Answers_Update::getInstance()->execute();
    }


    /**
     * Get list Answer (using for admin page)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\Answers_List::getInstance()->execute();
    }

    /**
     * Get all Answer (using for mobile)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\Answers_All::getInstance()->execute();
    }

    /**
     * Disable/Enable list Answer
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\Answers_Disable::getInstance()->execute();
    }

    /**
     * Get detail Answer
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\Answers_Detail::getInstance()->execute();
    }

    /**
     * Update field nice_count for Answer
     *
     * @return boolean
     */
    public function action_updateCounter()
    {
        return \Bus\Answers_UpdateCounter::getInstance()->execute();
    }
    
    /**
     * Approve list Answers.
     *
     * @return boolean
     */
    public function action_approve()
    {
        return \Bus\Answers_Approve::getInstance()->execute();
    }
    
    /**
     * Reject list Answers.
     *
     * @return boolean
     */
    public function action_reject()
    {
        return \Bus\Answers_Reject::getInstance()->execute();
    }
}
