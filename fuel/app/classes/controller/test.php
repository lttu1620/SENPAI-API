<?php

/**
 * Controller_Test
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
class Controller_Test extends \Controller_Rest {

    public function action_getauth() {
        $gmtime = \Lib\Util::gmtime(date('Y/m/d H:i:s'));
        echo '<br/>senpai:';
        echo '<br/>api_auth_date = ' . $gmtime; 
        echo '<br/>api_auth_key = ' . hash('md5', Config::get('api_secret_key') . $gmtime);
        echo '<br/>-----------------------------------------------------------------';
        echo '<br/>smarthighschool:'; 
        echo '<br/>date = ' . $gmtime; 
        echo '<br/>token = ' . hash('md5', Config::get('smarthighschool_salt') . $gmtime);
        echo '<br/>-----------------------------------------------------------------';
        echo '<br/>GMT+0: ' . date('Y-m-d H:i:s', $gmtime);        
    }
    
    public function action_checklog($d = 0) { 
        include_once APPPATH . "/config/auth.php";
        if (empty($d)) {
            $d = date('d');
        }
        $rootpath = \Config::get('log_path').date('Y').'/';
        $filepath = \Config::get('log_path').date('Y/m').'/';
        $filename = $filepath . $d . '.php';
        if (!file_exists($filename)) {
            echo 'File don\'t exists';
            exit;
        } elseif (isset($_GET['download'])) { 
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename='.basename($filename));
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . filesize($filename));            
        }
        $handle = fopen($filename, "r") or die("Unable to open file!");
        $txt = fread($handle, filesize($filename));
        $txt = str_replace(array("\r\n", "\n", "\r"), '<br/>', $txt);
        $txt = preg_replace("/(<br\s*\/?>\s*)+/", "<br/>", $txt);        
        fclose($handle);
        p($txt, 1);
    }

}
