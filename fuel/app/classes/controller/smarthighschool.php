<?php

/**
 * Controller for actions on admin
 *
 * @package Controller
 * @created 2014-11-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_SmartHighSchool extends \Controller_App {
    
    /**
     * Get notification
     *
     * @return boolean
     */
    public function action_notice() {
        return \Bus\SmartHighSchool_Notice::getInstance()->execute();
    }    

}
