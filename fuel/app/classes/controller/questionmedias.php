<?php

/**
 * Controller for actions on Question Media
 *
 * @package Controller
 * @created 2015-03-20
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
class Controller_QuestionMedias extends \Controller_App
{
    
    
    /**
     * Get all Question Media (using array count)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\QuestionMedias_All::getInstance()->execute();
    }
    
    
    /**
     * Get detail Question Media (using array count)
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\QuestionMedias_Detail::getInstance()->execute();
    }
    
    /**
     * Add info for Question Media
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\QuestionMedias_Add::getInstance()->execute();
    }


    /**
     * Disable/Enable a Question Media
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\QuestionMedias_Disable::getInstance()->execute();
    }
}
