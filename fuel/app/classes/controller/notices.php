<?php

/**
 * Controller for actions on Notices
 *
 * @package Controller
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Controller_Notices extends \Controller_App
{
     /**
     * Get all Notices
     *  
     * @return boolean    
     */
    public function action_all() {
        return \Bus\Notices_All::getInstance()->execute();
    }
 
    /**
     * Get detail Notices
     *  
     * @return boolean    
     */
    public function action_detail() {
        return \Bus\Notices_Detail::getInstance()->execute();
    }
    
    /**
     * Get List Notices by conditions
     *  
     * @return boolean    
     */
    public function action_list() {
        return \Bus\Notices_List::getInstance()->execute();
    }
    
    /**
     * Update disable field for Notices
     *  
     * @return boolean    
     */
    public function action_disable() {
        return \Bus\Notices_Disable::getInstance()->execute();
    }

    /**
     * Add new information for Notices
     *  
     * @return boolean    
     */
    public function action_add() {
        return \Bus\Notices_Add::getInstance()->execute();
    }

    /**
     * Check read for Notices
     *
     * @return boolean
     */
    public function action_isRead() {
        return \Bus\Notices_IsRead::getInstance()->execute();
    }
}