<?php

/**
 * Controller for actions on User Group
 *
 * @package Controller
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_UserGroups extends \Controller_App
{
    /**
     * Add or update info for User Group
     *
     * @return boolean
     */
    public function action_addUpdate()
    {
        return \Bus\UserGroups_AddUpdate::getInstance()->execute();
    }

    /**
     * Get list User Group (using for admin page)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\UserGroups_List::getInstance()->execute();
    }

    /**
     * Get all User Group (using for mobile)
     *
     * @return boolean
     */
    public function action_all()
    {
        return \Bus\UserGroups_All::getInstance()->execute();
    }

    /**
     * Get detail User Group
     *
     * @return boolean
     */
    public function action_detail()
    {
        return \Bus\UserGroups_Detail::getInstance()->execute();
    }
    
    /**
     * Enable/disable user_groups.
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\UserGroups_Disable::getInstance()->execute();
    }
}
