<?php

/**
 * Controller of Answernices
 *
 * @package Controller
 * @version 1.0
 * @author t
 * @copyright Oceanize INC
 */
class Controller_Answernices extends \Controller_App {
    
    /**
     * Get list of answernices by id
     *
     * @return boolean
     */
    public function action_list() {
        return \Bus\Answernices_List::getInstance()->execute();
    }
    /**
     * Set disable value for answernice
     *  
     * @return boolean   
     */
    public function action_disable() {
        return \Bus\Answernices_Disable::getInstance()->execute();
    }

    /**
     * Update infomation for answernice
     *  
     * @return boolean   
     */
    public function action_add() {
        return \Bus\Answernices_Add::getInstance()->execute();
    }

  }
