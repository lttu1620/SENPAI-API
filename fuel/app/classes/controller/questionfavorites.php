<?php

/**
 * Controller for actions on Question Favorite
 *
 * @package Controller
 * @created 2015-03-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_QuestionFavorites extends \Controller_App
{
    /**
     * Add info for Question Favorite
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\QuestionFavorites_Add::getInstance()->execute();
    }

    /**
     * Get list Question Favorite (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\QuestionFavorites_List::getInstance()->execute();
    }

    /**
     * Disable/Enable a Question Favorite
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\QuestionFavorites_Disable::getInstance()->execute();
    }
}
