<?php

/**
 * Controller for actions on Question Nice
 *
 * @package Controller
 * @created 2015-03-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_QuestionNices extends \Controller_App
{
    /**
     * Add info for Question Nice
     *
     * @return boolean
     */
    public function action_add()
    {
        return \Bus\QuestionNices_Add::getInstance()->execute();
    }

    /**
     * Get list Question Nice (using array count)
     *
     * @return boolean
     */
    public function action_list()
    {
        return \Bus\QuestionNices_List::getInstance()->execute();
    }

    /**
     * Disable/Enable a Question Nice
     *
     * @return boolean
     */
    public function action_disable()
    {
        return \Bus\QuestionNices_Disable::getInstance()->execute();
    }
}
