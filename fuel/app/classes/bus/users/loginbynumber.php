<?php

namespace Bus;

/**
 * Users_Detail - API to get detail of Users
 *
 * @package Bus
 * @version 1.0
 * @author Caolp
 * @copyright Oceanize INC
 */
class Users_LoginByNumber extends BusAbstract {

    /** @var array $_required field require */
    protected $_required = array(
        'number',
    );
    /** @var array $_length Length of fields */
    protected $_number_format = array(
    );
    /** @var array $_number_format field number */
    protected $_length = array(
    );

    /**
     * Call function login() from model Users
     *
     * @author Caolp
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data) {
        try {
            $result = \Model_User::login_by_number($data);
            if (!empty($result)) {
                $result['token'] = \Model_Authenticate::addupdate(array(
                    'user_id' => $result['id'],
                    'regist_type' => 'user'
                ));
            }
            $this->_response = $result;
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
