<?php

namespace Bus;

/**
 * API to get list of Users
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Users_AddUpdate extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'name', //user_id
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_group_id' => array(0, 11),
        'enrollment_id' => array(0, 11),
        'number' => array(0, 50),
        'password' => array(0, 256),
        'hash' => array(0, 256),
        'name' => array(0, 128),
        'grade' => array(0, 64),
        'mail' => array(0,128),
        'mail_parent' => array(0,128),
        'last_login_date' => array(0,11),
        'type' => array(0,11),
        'image_url' => array(0, 128),
    );
    
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_group_id',
        'sex_id'
    );
    
    /** @var array $_date_format field date */
    protected $_date_format = array(
        'last_login_date' => 'Y-m-d',
    );
    
     /** @var array $_number_format field number */
    protected $_email_format = array('mail');
    
    /**
     * Call function detail from model Users
     *
     * @author truongnn
     * @param array $data Input data
     * @return bool|int Returns the array or the integer.
     */
    public function operateDB($data)
    {
        try
        {            
            $this->_response = \Model_User::add_update($data);
            return $this->result(\Model_User::error());
        } 
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
