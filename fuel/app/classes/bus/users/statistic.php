<?php

namespace Bus;

/**
 *  API to statistic for user by user_id.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Users_Statistic extends BusAbstract
{
     /** @var array $_required field require */
    protected $_required = array(
        'user_id',
    );
    
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id'
    );
    
    /**
     * Count answer/question/question_favorite/question_nice/answer_nice by user_id.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Returns the array.
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User::get_statistic($data);
            return $this->result(\Model_User::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
