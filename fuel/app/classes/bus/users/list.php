<?php

namespace Bus;

/**
 *  API to get list of Users
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Users_List extends BusAbstract
{

    protected $_length = array(
        'user_group_id' => array(0, 11),
        'enrollment_id' => array(0, 11),
        'number' => array(0, 11),
        'grade' => array(0, 64),
        'mail' => array(0, 128),
    );
    
    //check number
    protected $_number_format = array(
        'user_group_id',
        'enrollment_id',
        'number',
        'page',
        'limit',
    );
    
    protected $_email_format = array(
        'mail',
    );
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User::get_list($data);
            return $this->result(\Model_User::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
