<?php

namespace Bus;

/**
 *  API to profile for user by user_id.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Users_Profile extends BusAbstract
{
     /** @var array $_required field require */
    protected $_required = array(
        'user_id',
    );
    
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id'
    );
    
    /**
     * Get profile by user_id.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Return information of user.
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User::get_profile($data);
            return $this->result(\Model_User::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
