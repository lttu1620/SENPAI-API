<?php

namespace Bus;

/**
 * API to disable or enable a Users.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Users_Disable extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'disable',
    );
    
    /** @var array $_length Length of fields */
    protected $_length = array(
        'disable' => 1,
    );
    
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable',
    );
    /**
     * Call function enable/disable from model Users.
     *
     * @author Le Tuan Tu
     * @param array $data Input data.
     * @return bool Success or otherwise.
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User::disable($data);
            return $this->result(\Model_User::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
