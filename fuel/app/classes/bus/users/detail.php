<?php

namespace Bus;

/**
 * API to get detail of Users.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Users_Detail extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id', //user_id
    );
    
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(0, 11),
    );
    
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
    );
    
    /**
     * Call function detail from model Users
     *
     * @author truongnn
     * @param array $data Input data
     * @return array Returns the array.
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User::get_detail($data);
            return $this->result(\Model_User::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
