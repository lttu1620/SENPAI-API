<?php

namespace Bus;

/**
 * Get detail Question
 *
 * @package Bus
 * @created 2015-04-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Tadacopy_Questions_Detail extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'key',
        'tadacopy_app_id',
        'date',
        'question_id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'question_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'question_id'
    );

    /**
     * Call function get_detail_for_tadacopy() from model Question
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Question::get_detail_for_tadacopy($data);
            return $this->result(\Model_Question::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
