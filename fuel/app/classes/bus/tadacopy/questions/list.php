<?php

namespace Bus;

/**
 * Get list Question
 *
 * @package Bus
 * @created 2015-04-20
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Tadacopy_Questions_List extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'key',
        'tadacopy_app_id',
        'date'
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit'
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'page'  => '1',
        'limit' => '15'
    );

    /**
     * Call function get_list() from model Question
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Question::get_list_for_tadacopy($data);
            return $this->result(\Model_Question::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
