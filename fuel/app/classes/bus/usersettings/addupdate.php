<?php

namespace Bus;

/**
 * UserSettings_AddUpdate - Model to operate to UserSettings's functions.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class UserSettings_AddUpdate extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id',
        'value'
    );
    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id' => array(1, 11),
        'setting_id' => array(1, 11),
    );
    /** @var array $_number_format field number */
    protected $_number_format= array(
        'user_id',
        'setting_id',
    );

    /**
     * Add or update user_setting.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool|int Returns the boolean or the integer.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Setting::add_update($data);
            return $this->result(\Model_User_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
