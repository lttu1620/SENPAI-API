<?php

namespace Bus;

/**
 * UserSettings_MultiUpdate - Model to operate to UserSettings's functions.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class UserSettings_MultiUpdate extends BusAbstract
{
     /** @var array $_required field require */
    protected $_required = array(
        'value'
    );
    /**
     * Update multi_user_setting.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Setting::multi_update($data);
            return $this->result(\Model_User_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
