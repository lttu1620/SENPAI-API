<?php

namespace Bus;

/**
 * UserSettings_All - Model to operate to UserSettings's functions.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class UserSettings_All extends BusAbstract {
     /** @var array $_required field require */
    protected $_required = array(
        'user_id',
    );
    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id' => array(1, 11),
    );
    /** @var array $_number_format field number */
    protected $_number_format= array(
        'user_id',
    );
    
    /**
     * Get all information of user_setting.
     *
     * @author truongnn
     * @param array $data Input array
     * @return array Returns array(total, data).
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Setting::get_all($data);
            return $this->result(\Model_User_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
