<?php

namespace Bus;

/**
 * Get all Category (without array count)
 *
 * @package Bus
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Categories_All extends BusAbstract
{
    /**
     * Call function get_all() from model Category
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Category::get_all($data);
            return $this->result(\Model_Category::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
