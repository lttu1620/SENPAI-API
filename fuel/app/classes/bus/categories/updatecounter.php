<?php

namespace Bus;

/**
 * Update field question_count for Category
 *
 * @package Bus
 * @created 2015-03-18
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Categories_UpdateCounter extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'category_id'
    );

    /**
     * Call function update_counter() from model Category
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Category::update_counter($data);
            return $this->result(\Model_Category::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
