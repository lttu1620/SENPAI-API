<?php

namespace Bus;

/**
 * Get all campuses.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class SmartHighSchool_Notice extends BusAbstract
{    
    /** @var array $_required field require */
    protected $_required = array(
        'number',
        'date',
        'token',
    );
    
     /** @var array $_number_format field number */
    protected $_number_format = array(
        'date',
        'limit'
    );
    
    /**
     * Call function get_all() from model notice.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array List notices.
     */
    public function operateDB($data)
    {
        try {
            // check valid date 
            // if request date before n minutes (n = Config::get('smarthighschool_request_minute','))
            if ($data['date'] < \Lib\Util::gmtime(date("Y-m-d H:i:s", strtotime('-' . \Config::get('smarthighschool_request_minute', 15) . 'minutes')))) 
            {
                $this->_addError(self::ERROR_CODE_AUTH_ERROR, 'date');
                return $this->getResponse(self::ERROR_CODE_AUTH_ERROR);
            }          
            // check valid key
            // api_auth_key = md5(smarthighschool_salt + date)
            if (hash('md5', \Config::get('smarthighschool_salt') . $data['date']) != $data['token']) 
            {   
                $this->_addError(self::ERROR_CODE_AUTH_ERROR, 'token');
                return $this->getResponse(self::ERROR_CODE_AUTH_ERROR);
            }
            $this->_response = \Model_Notice::get_for_smarthighschool($data);
            return $this->result(\Model_Notice::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
