<?php

namespace Bus;

/**
 * Get all Segment (using for mobile)
 *
 * @package Bus
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Segments_All extends BusAbstract
{
    /**
     * Call function get_all() from model Segment
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Segment::get_all();
            return $this->result(\Model_Segment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
