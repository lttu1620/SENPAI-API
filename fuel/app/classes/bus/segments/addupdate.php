<?php

namespace Bus;

/**
 * Add or update info for Segment
 *
 * @package Bus
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Segments_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'        => array(1, 11),
        'name'      => array(1, 128)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id'
    );

    /**
     * Call function add_update() from model Segment
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Segment::add_update($data);
            return $this->result(\Model_Segment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
