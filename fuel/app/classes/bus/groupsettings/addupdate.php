<?php

namespace Bus;

/**
 * <GroupSettings_AddUpdate - Model to operate to GroupSettings's functions>
 *
 * @package Bus
 * @created 2014-12-15
 * @updated 2014-12-15
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class GroupSettings_AddUpdate extends BusAbstract
{
    protected $_required = array(
        'group',
        'value'
    );
    /**
     * call function add_update()
     *
     * @created 2014-12-15
     * @updated 2014-12-15
     * @access public
     * @author <diennvt>
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Group_Setting::add_update($data);
            return $this->result(\Model_Group_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
