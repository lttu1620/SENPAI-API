<?php

namespace Bus;

/**
 * Add or update info answer_nices.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Answernices_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'answer_id',
        'user_id'
    );
    /** @var array $_length Length of fields */
    protected $_length = array(
        'answer_id' => array(1, 11),
        'user_id' => array(1, 11),
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'answer_id',
        'user_id'
    );
    
    /**
     * Call function add() from model answer_nices.
     *
     * @author truongnn
     * @param $data
     * @param array $data Input array.
     * @return bool|int Returns the boolean or the the array. 
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Answer_Nice::add($data);
            return $this->result(\Model_Answer_Nice::error());            
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}