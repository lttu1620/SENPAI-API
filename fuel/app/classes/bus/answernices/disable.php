<?php

namespace Bus;

/**
 * Enable/Disable answer_nices.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Answernices_Disable extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(1, 11), 
        'answer_id' => array(1, 11),
        'user_id' => array(1, 11),
        'disable' => 1,
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'answer_id',
        'user_id',
        'disable',
    );

    /**
     * Call function disable() from model answer_nices.
     *
     * @author truongnn
     * @param array $data Input array
     * @return bool Returns the boolean.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Answer_Nice::disable($data);
            return $this->result(\Model_Answer_Nice::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
