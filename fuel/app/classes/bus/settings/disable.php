<?php

namespace Bus;

/**
 * Settings_Disable - Model to operate to Settings's functions.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Settings_Disable extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'disable',
    );
   /** @var array $_length Length of fields */
    protected $_length = array(
        'disable' => 1,
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable'
    );
    
    /**
     * Enable/disable setting.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Setting::disable($data);
            return $this->result(\Model_Setting::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
