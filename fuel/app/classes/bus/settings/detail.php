<?php

namespace Bus;

/**
 * <Settings_Detail - Model to operate to Settings's functions>
 *
 * @package Bus
 * @created 2014-12-12
 * @updated 2014-12-12
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Settings_Detail extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
    );
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(1, 11),
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
    );

    /**
     * Get detail setting.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Setting::get_detail($data);
            return $this->result(\Model_Setting::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
