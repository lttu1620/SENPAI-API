<?php

namespace Bus;

/**
 * Settings_List - Model to operate to Settings's functions.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Settings_List extends BusAbstract
{

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(1, 11),
        'name' => array(0, 100),
        'type' => array(0, 10),
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
    );

    /**
     * Get list settings. 
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Return array(total, data).
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Setting::get_list($data);
            return $this->result(\Model_Setting::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
