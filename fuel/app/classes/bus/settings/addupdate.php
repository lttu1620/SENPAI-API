<?php

namespace Bus;

/**
 * Settings_AddUpdate - Model to operate to Settings's functions.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Settings_AddUpdate extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'name',
        'type'
    );
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(1, 11),
        'name' => array(0, 100),
        'type' => array(0, 10),
    );
    
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
    );
    /**
     * Call function add_update() from model News Site.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool|int Returns the boolean or the integer.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Setting::add_update($data);
            return $this->result(\Model_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
