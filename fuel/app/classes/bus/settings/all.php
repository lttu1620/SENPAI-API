<?php

namespace Bus;

/**
 * Settings_All - Model to operate to Settings's functions.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Settings_All extends BusAbstract {
    /** @var array $_required field require */
    protected $_required = array(
        'type',
    );
    /** @var array $_length Length of fields */
    protected $_length = array(
        'type' => array(0, 10),
    );
    /**
     * Get all information of setting.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Returns array(total, data).
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Setting::get_all($data);
            return $this->result(\Model_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
