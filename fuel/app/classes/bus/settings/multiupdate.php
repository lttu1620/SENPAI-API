<?php

namespace Bus;

/**
 * Settings_MultiUpdate - Model to operate to Settings's functions.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Settings_MultiUpdate extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'value',
    );
    /**
     * Call function multi_update() from model News Site.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Setting::multi_update($data);
            return $this->result(\Model_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
