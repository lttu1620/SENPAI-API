<?php

namespace Bus;

/**
 * Get list Question (using for admin page)
 *
 * @package Bus
 * @created 2015-03-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Questions_Top extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'login_user_id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'login_user_id'
    );

    /**
     * Call function get_top() from model Question
     *
     * @author Lai Hac Thai
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Question::get_top($data);
            return $this->result(\Model_Question::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
