<?php

namespace Bus;

/**
 * Update field answer_count, nice_count and favorite_count for Question
 *
 * @package Bus
 * @created 2015-03-18
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Questions_UpdateCounter extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'question_id'
    );

    /**
     * Call function update_counter() from model Question
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Question::update_counter($data);
            return $this->result(\Model_Question::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
