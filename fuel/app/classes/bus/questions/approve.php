<?php

namespace Bus;

/**
 * Unapprove/Approve list Question
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Questions_Approve extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id'
    );

    /**
     * Approve Questions.
     *
     * @author truongnn
     * @param array $data Input data.
     * @return bool Success or otherwise.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Question::approve($data);
            return $this->result(\Model_Question::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
