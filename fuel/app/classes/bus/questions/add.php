<?php

namespace Bus;

/**
 * Add info for Question
 *
 * @package Bus
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Questions_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id',
        'category_id',
        'content',
        'to_univ',
        'to_high',
        'to_teacher'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id'     => array(1, 11),
        'category_id' => array(1, 11),
        'to_univ'     => 1,
        'to_high'     => 1,
        'to_teacher'  => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'category_id',
        'to_univ',
        'to_high',
        'to_teacher'
    );

    /**
     * Call function add() from model Question
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Question::add($data);
            return $this->result(\Model_Question::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
