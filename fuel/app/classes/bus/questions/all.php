<?php

namespace Bus;

/**
 * Get all Question (using for mobile)
 *
 * @package Bus
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Questions_All extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'login_user_id' => array(1, 11),
        'status'        => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'login_user_id',
        'status'
    );

    /**
     * Call function get_all() from model Question
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Question::get_all($data);
            return $this->result(\Model_Question::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
