<?php

namespace Bus;

/**
 * Get list Question (using for admin page)
 *
 * @package Bus
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Questions_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'category_id'   => array(1, 128),
        'user_id'       => array(1, 11),
        'login_user_id' => array(1, 11),
        'user_number'   => array(1, 11),
        'disable'       => 1,
        'status'        => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'category_id',
        'user_id',
        'login_user_id',
        'user_number',
        'disable',
        'status'
    );

    /**
     * Call function get_list() from model Question
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Question::get_list($data);
            return $this->result(\Model_Question::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
