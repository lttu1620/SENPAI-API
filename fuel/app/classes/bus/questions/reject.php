<?php

namespace Bus;

/**
 *Reject list Question
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Questions_Reject extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id'
    );

    /**
     * Reject Questions.
     *
     * @author truongnn
     * @param array $data Input data.
     * @return bool Success or otherwise.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Question::reject($data);
            return $this->result(\Model_Question::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
