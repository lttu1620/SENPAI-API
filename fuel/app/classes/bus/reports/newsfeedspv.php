<?php

namespace Bus;

/**
 * <Reports_NewsFeedsPv - API to get statistical data of NewsFeeds>
 *
 * @package Bus
 * @created 2014-11-20
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Reports_NewsFeedsPv extends BusAbstract
{
    //check length
    protected $_length = array(
        'news_feed_id' => array(1, 11),
    );
    //check number
    protected $_number_format = array(
        'news_feed_id',
    );
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Feed_View_Log::get_pv_report($data);
            return $this->result(\Model_News_Feed_View_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
