<?php

namespace Bus;

/**
 * <get list data to report news_feed_favorites>
 *
 * @package Bus
 * @created 2014-12-26
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Reports_NewsFeedFavorites extends BusAbstract
{
    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );
	/**
	 * <get list data to report news_feed_favorites>
	 *
	 * @created 2014-12-26
	 * @updated 2014-12-26
	 * @access public
	 * @author truongnn
	 * @param $data
	 * @return bool
	 */
	public function operateDB($data)
	{
		try {
			$this->_response = \Model_News_Feed_Favorite::get_countFavorite($data);
			return $this->result(\Model_News_Feed_Favorite::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}
