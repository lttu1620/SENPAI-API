<?php

namespace Bus;

/**
 * get share type for news feed
 *
 * @package Bus
 * @created 2014-12-25
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Reports_NewsFeedShares extends BusAbstract
{

    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    /**
     * get share type for news feed
     *
     * @created 2014-12-25
     * @updated 2014-12-25
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Share_Log::get_share_report($data);
            return $this->result(\Model_Share_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
