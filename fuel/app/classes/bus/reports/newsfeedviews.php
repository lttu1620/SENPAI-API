<?php

namespace Bus;

/**
 * get report news feed view
 *
 * @package Bus
 * @created 2014-12-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Reports_NewsFeedViews extends BusAbstract
{

    //check length
    protected $_length = array(
        'disable' => 1,
    );
    //check number
    protected $_number_format = array(
        'disable',
    );
    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    /**
     * get report news feed view
     *
     * @created 2014-12-26
     * @updated 2014-12-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Feed_View_Log::get_view_report($data);
            return $this->result(\Model_News_Feed_View_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
