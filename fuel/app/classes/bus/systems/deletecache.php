<?php
namespace Bus;
/**
 * add or update info for admin
 *
 * @package Bus
 * @created 2015-01-14
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Systems_DeleteCache extends BusAbstract {

    /**
     * call function delete_cache() from model System
     *
     * @created 2015-01-14
     * @updated 2015-01-14
     * @access public
     * @author truongnn
     * @param $data
     * @return bool
     */
    public function operateDB($data) {
        try {
            $keys = \Config::get('cache.key');
            if (!empty($keys)) {
                foreach ($keys as $key => $value) {
                    switch ($key) {
                        case 'setting_all':
                            \Lib\Cache::delete($key . '_global');
                            \Lib\Cache::delete($key . '_user');
                            \Lib\Cache::delete($key . '_company');
                            \Lib\Cache::delete($key . '_admin');                     
                            break;                       
                        default :
                            \Lib\Cache::delete($key);
                    }
                }                
            }   
            return true;
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
