<?php

namespace Bus;

/**
 * add or update info for tags
 *
 * @package Bus
 * @created 2015-01-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Tags_AddUpdate extends BusAbstract
{
    //check length
    protected $_length = array(
        'id'         => array(1, 11),
        'name'       => array(1, 50),
        'color'      => array(0, 10),
        'feed_count' => array(0, 11)
    );

    //check number
    protected $_number_format = array(
        'id',
        'feed_count'
    );

    // set default value
    protected $_default_value = array(
        'feed_count' => '0'
    );

    /**
     * call function add_update() from model Tags
     *
     * @created 2015-01-16
     * @updated 2015-01-16
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Tag::add_update($data);
            return $this->result(\Model_Tag::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
