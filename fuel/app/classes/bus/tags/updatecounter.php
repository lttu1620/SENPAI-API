<?php

namespace Bus;

/**
 * update counter for tag
 *
 * @package Bus
 * @created 2015-01-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Tags_UpdateCounter extends BusAbstract
{
    //check require
    protected $_required = array(
        'id'
    );

    /**
     * call function update_counter() from model Tag
     *
     * @created 2015-01-16
     * @updated 2015-01-16
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool|true
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Tag::update_counter($data);
            return $this->result(\Model_Tag::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
