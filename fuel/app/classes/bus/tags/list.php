<?php

namespace Bus;

/**
 * get list tag
 *
 * @package Bus
 * @created 2015-01-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Tags_List extends BusAbstract
{
    //check length
    protected $_length = array(
        'name'       => array(1, 50),
        'color'      => array(0, 10)
    );

    //check number
    protected $_number_format = array(
        'disable',
        'page',
        'limit'
    );

    /**
     * call function get_list() from model Tag
     *
     * @created 2015-01-16
     * @updated 2015-01-16
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Tag::get_list($data);
            return $this->result(\Model_Tag::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
