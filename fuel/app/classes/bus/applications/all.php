<?php

namespace Bus;

/**
 * Get list Applications
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Applications_All extends BusAbstract
{

    /**
     * Call function get_all() from model enrollment.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Returns the array.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Application::get_all($data);
            return $this->result(\Model_Application::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
