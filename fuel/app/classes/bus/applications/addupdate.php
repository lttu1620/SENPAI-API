<?php

namespace Bus;

/**
 * add or update info for application
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Applications_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name' => array(1, 128),
        'url' => array(1, 256),
        'started' => array(1, 11),
        'finished' => array(1, 11),
        'priority' => array(1,11),
        'segment_id' => array(1,11),
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'priority',
        'segment_id'
    );
     /** @var array $_date_format field date */
    protected $_date_format = array(
        'started' => 'Y-m-d',
        'finished' => 'Y-m-d'
    );
     /** @var array $_url_format field url */
    protected $_url_format = array(
        'url'
    );
    
    /**
     * Call function add_update() from model Application.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool|int Returns the boolean or the integer.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Application::add_update($data);
            return $this->result(\Model_Application::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}