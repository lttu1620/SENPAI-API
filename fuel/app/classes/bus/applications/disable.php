<?php

namespace Bus;

/**
 * Enable/Disable Application.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Applications_Disable extends BusAbstract
{
     protected $_required = array(
        'id',
         'disable'
    );
    
    //check length
    protected $_length = array(        
        'disable' => 1
    );
    
    //check number
    protected $_number_format = array(       
        'disable'
    );
    /**
     * Call function disable() from model Application.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Application::disable($data);
            return $this->result(\Model_Application::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
