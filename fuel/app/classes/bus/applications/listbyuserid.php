<?php

namespace Bus;

/**
 * Get list application.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Applications_ListByUserId extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id'
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
        'page',
        'limit'
    );

    /**
     * Call function get_list() from model Application.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Returns the array.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Application::get_list_by_user_id($data);
            return $this->result(\Model_Application::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
