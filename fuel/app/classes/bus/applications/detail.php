<?php

namespace Bus;

/**
 * Get detail application
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Applications_Detail extends BusAbstract {

    /** @var array $_required field require */
    protected $_required = array(
        'id'
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id'
    );

    /**
     * Get detail enrollments.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Returns the array.
     */
    public function operateDB($data) {
        try {
            $this->_response = \Model_Application::get_detail($data);
            return $this->result(\Model_Application::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
