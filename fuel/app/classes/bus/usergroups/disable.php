<?php

namespace Bus;

/**
 * API to disable or enable a Usergroups.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Usergroups_Disable extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'disable',
    );
    
    /** @var array $_length Length of fields */
    protected $_length = array(
        'disable' => 1,
    );
    
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable',
    );
    /**
     * Call function enable/disable from model Usergroups.
     *
     * @authortruongnn
     * @param array $data Input data.
     * @return bool Success or otherwise.
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User_Group::disable($data);
            return $this->result(\Model_User_Group::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
