<?php

namespace Bus;

/**
 * Get all User Group (using for mobile)
 *
 * @package Bus
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class UserGroups_All extends BusAbstract
{
    /**
     * Call function get_all() from model User Group
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User_Group::get_all();
            return $this->result(\Model_User_Group::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
