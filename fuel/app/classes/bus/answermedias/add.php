<?php

namespace Bus;

/**
 * Add info for Answer Media
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
class AnswerMedias_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'answer_id',
        'user_id',
        'media_url'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'answer_id' => array(1, 11),
        'user_id'     => array(1, 11),
        'media_url'     => array(1, 255),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'answer_id',
        'user_id'
    );
     /** @var array $_url_format field url */
    protected $_url_format = array(
        'media_url'
    );


    /**
     * Call function add() from model Answer Media
     *
     * @author caolp
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Answer_Media::add($data);
            return $this->result(\Model_Answer_Media::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}