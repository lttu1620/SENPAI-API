<?php

namespace Bus;

/**
 * Add info for Answer Media
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
class AnswerMedias_All extends BusAbstract
{
    /**
     * Call function add() from model Answer Media
     *
     * @author caolp
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Answer_Media::get_all($data);
            return $this->result(\Model_Answer_Media::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}