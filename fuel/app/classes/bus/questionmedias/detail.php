<?php

namespace Bus;

/**
 * Add info for Question Media
 *
 * @package Bus
 * @created 2015-03-20
 * @version 1.0
 * @author caolp
 * @copyright Oceanize INC
 */
class QuestionMedias_Detail extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(1, 11)
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
    );

    /**
     * Call function detail() from model Question Media
     *
     * @author caolp
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Question_Media::get_detail($data);
            return $this->result(\Model_Question_Media::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}