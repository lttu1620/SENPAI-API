<?php

namespace Bus;

/**
 * Fe_Usersettings_List - API to get list of Fe Usersettings on frontent
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Fe_Usersettings_List extends BusAbstract {
    
     /** @var array $_required field require */
    protected $_required = array(
        'user_id'
    );
    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id' => array(1, 11),
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
    );
    
    /**
     * Get list usersettings by user_id for frontend.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Returns the array.
     */
    public function operateDB($data) {
        try {
            $this->_response = \Model_User_Setting::fe_get_list($data);
            return $this->result(\Model_User_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
