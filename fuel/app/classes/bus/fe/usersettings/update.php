<?php

namespace Bus;

/**
 * Fe_UserSettings_Update - API to Update of UserSettings on mobile.
 *
 * @package Bus
 * @created 2014-12-19
 * @version 1.0
 * @author <tuancd>
 * @copyright Oceanize INC
 */
class Fe_UserSettings_Update extends BusAbstract {

    protected $_required = array(
        'user_id',
        'setting_id',
        'value'
    );
    
    protected $_length = array(
        'user_id' => array(1, 11),
    );
    
    protected $_number_format = array(
        'user_id',
        'setting_id'
    );
    /**
     * Update usersettings.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Returns the array.
     */
    public function operateDB($data) {
        try {
            $this->_response = \Model_User_Setting::fe_update($data);
            return $this->result(\Model_User_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
