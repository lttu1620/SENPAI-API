<?php

namespace Bus;

/**
 * Get detail user for frontend.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Fe_Users_Detail extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id'
    );
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id' => array(1, 11),
        'user_id' => array(1, 11),
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'user_id'
    );
    /**
     * Get detail user by user_id for frontend.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Returns the array.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_User::get_detail_for_mobile($data);
            return $this->result(\Model_User::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
