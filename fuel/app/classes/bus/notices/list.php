<?php

namespace Bus;

/**
 * get list campuses
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Notices_List extends BusAbstract
{
     /** @var array $_length Length of fields */
    protected $_length = array(
        'question_id' => array(1, 11),
        'answer_id' => array(1, 11),
        'user_id' => array(1, 11),
        'type' => array(1, 11)
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'question_id',
        'answer_id',
        'user_id',
        'type'
    );

    /**
     * Call function get_list() from model Notice
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array List notices.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Notice::get_list($data);
            return $this->result(\Model_Notice::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
