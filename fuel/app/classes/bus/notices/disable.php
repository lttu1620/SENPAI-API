<?php

namespace Bus;

/**
 * Enable/Disable Notice
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Notices_Disable extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id',
        'disable',
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'disable'
    );

    // check length
    protected $_length = array(
        'disable' => 1
    );
    
    /**
     * Disable/enable from model Notice
     *
     * @author truongnn
     * @param $data
     * @return bool True if update successfully or otherwise.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Notice::disable($data);
            return $this->result(\Model_Notice::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
