<?php

namespace Bus;

/**
 * Add info for Notices.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Notices_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'question_id',
        'user_id',
        'type'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'question_id' => array(1, 11),
        'answer_id' => array(1, 11),
        'user_id' => array(1, 11),
        'type' => array(1, 11)
    );
    
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'question_id',
        'answer_id',
        'user_id',
        'type'
    );

    /** @var array $_default_value field default */
    protected $_default_value = array(
        'is_read' => '0'
    );

    /**
     * Call function add() from model Notice.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool|int Id of object campus if add or update successfully otherwise return false. 
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Notice::add($data);
            return $this->result(\Model_Notice::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
