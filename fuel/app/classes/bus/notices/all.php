<?php

namespace Bus;

/**
 * Get all campuses.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Notices_All extends BusAbstract
{
    /**
     * Call function get_all() from model notice.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array List notices.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Notice::get_all($data);
            return $this->result(\Model_Notice::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
