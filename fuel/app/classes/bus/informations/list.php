<?php

namespace Bus;

/**
 * Get list information.
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Informations_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'title' => array(1, 128),
        'content' => array(1, 256),
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit'
    );

    /**
     * Call function get_list() from model Information.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Returns the array.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Information::get_list($data);
            return $this->result(\Model_Information::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
