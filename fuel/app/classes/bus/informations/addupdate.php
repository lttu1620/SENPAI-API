<?php

namespace Bus;

/**
 * Add or update info for information
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Informations_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'title' => array(1, 128),
        'content' => array(1, 256),
        'started' => array(1, 11),
        'finished' => array(1, 11),
        'print' => array(1, 1),
        'segment_id' => array(1, 1)
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'print',
        'segment_id'
    );
     /** @var array $_date_format field date */
    protected $_date_format = array(
        'started' => 'Y-m-d',
        'finished' => 'Y-m-d'
    );
    /**
     * Call function add_update() from model Information.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool|int Returns the boolean or the integer.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Information::add_update($data);
            return $this->result(\Model_Information::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}