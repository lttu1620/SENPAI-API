<?php

namespace Bus;

/**
 * Get list Informations.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Informations_All extends BusAbstract
{

    /**
     * Call function get_all() from model information.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Returns the array.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Information::get_all($data);
            return $this->result(\Model_Information::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
