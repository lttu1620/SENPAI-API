<?php

namespace Bus;

/**
 * The print action of Information.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Informations_Printed extends BusAbstract
{
     protected $_required = array(
        'id',
         'print'
    );
    
    //check length
    protected $_length = array(        
        'print' => 1
    );
    
    //check number
    protected $_number_format = array(       
        'print'
    );
    /**
     * Call function print() from model Information.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Returns the boolean.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Information::printed($data);
            return $this->result(\Model_Information::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
