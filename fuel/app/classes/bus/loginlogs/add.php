<?php

namespace Bus;

/**
 * Add info for login logs.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class LoginLogs_Add extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'user_id',
    );
   /** @var array $_length Length of fields */
    protected $_length = array(
        'user_id' => array(1, 11)
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_id',
    );

    /**
     * Call function add() from model Login Logs.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return int|bool Returns the integer or the boolean.
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Login_Log::add($data);
            return $this->result(\Model_Login_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
