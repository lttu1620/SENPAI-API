<?php

namespace Bus;

/**
 * Get list login logs
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class LoginLogs_List extends BusAbstract
{

    /** @var array $_length Length of fields */
    protected $_length = array(
        'user_number' => array(0, 40),
        'mail' => array(1, 128)
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'user_number',
        'page',
        'limit'
    );
   /** @var array $_number_format field number */
    protected $_email_format = array(
        'mail',
    );
    /** @var array $_number_format field number */
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    /**
     * Call function get_list() from model login logs.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Return the array.
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Login_Log::get_list($data);
            return $this->result(\Model_Login_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}
