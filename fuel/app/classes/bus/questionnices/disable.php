<?php

namespace Bus;

/**
 * Disable a Question
 *
 * @package Bus
 * @created 2015-03-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class QuestionNices_Disable extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'          => array(1, 11),
        'question_id' => array(1, 11),
        'user_id'     => array(1, 11),
        'disable'     => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
        'question_id',
        'user_id',
        'disable'
    );

    /**
     * Call function disable() from model Question Nice
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Question_Nice::disable($data);
            return $this->result(\Model_Question_Nice::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
