<?php

namespace Bus;

/**
 * Get list Question Favorite (using array count)
 *
 * @package Bus
 * @created 2015-03-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class QuestionFavorites_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'question_id' => array(1, 128),
        'user_id'     => array(1, 11),
        'disable'     => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'question_id',
        'user_id',
        'disable'
    );

    /**
     * Call function get_list() from model Question Favorite
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Question_Favorite::get_list($data);
            return $this->result(\Model_Question_Favorite::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
