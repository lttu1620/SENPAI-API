<?php

namespace Bus;

/**
 * Get detail enrollment.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Enrollments_Detail extends BusAbstract {

    /** @var array $_required field require */
    protected $_required = array(
        'id'
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id'
    );

    /**
     * Get detail enrollment.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Returns the array.
     */
    public function operateDB($data) {
        try {
            $this->_response = \Model_Enrollment::get_detail($data);
            return $this->result(\Model_Enrollment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
