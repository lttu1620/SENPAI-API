<?php

namespace Bus;

/**
 * Add or update info for enrollment
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Enrollments_AddUpdate extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name' => array(1, 128),
        'started' => array(1, 11),
        'finished' => array(1, 11)
    );
    /** @var array $_date_format field date */
    protected $_date_format = array(
        'started' => 'Y-m-d',
        'finished' => 'Y-m-d'
    );
    /**
     * Call function add_update() from model Enrollment.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool|int Returns the boolean or the integer.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Enrollment::add_update($data);
            return $this->result(\Model_Enrollment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}