<?php

namespace Bus;

/**
 * Get list enrollments.
 *
 * @package Bus
 * @created 2014-11-21
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Enrollments_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'name' => array(1, 128)
    );
    /** @var array $_number_format field number */
    protected $_number_format = array(
        'started',
        'page',
        'limit'
    );
    
    /**
     * Call function get_list() from model Enrollment.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Returns the array.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Enrollment::get_list($data);
            return $this->result(\Model_Enrollment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
