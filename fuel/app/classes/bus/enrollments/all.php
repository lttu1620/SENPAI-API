<?php

namespace Bus;

/**
 * Get list Enrollments.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Enrollments_All extends BusAbstract
{

    /**
     * Call function get_all() from model enrollment.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return array Returns the array.
     */
    public function operateDB($data)
    {
        try {
            $options['where'][] = array(
                'disable' => 0
            );
            $this->_response = \Model_Enrollment::find('all', $options);
            return $this->result(\Model_Enrollment::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}
