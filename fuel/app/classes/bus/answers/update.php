<?php

namespace Bus;

/**
 * Update info for Answer
 *
 * @package Bus
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Answers_Update extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id'
    );

    /** @var array $_length Length of fields */
    protected $_length = array(
        'id'          => array(1, 11),
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'id',
    );

    /**
     * Call function set_update() from model Answer
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Answer::set_update($data);
            return $this->result(\Model_Answer::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
