<?php

namespace Bus;

/**
 * Reject list Answers.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Answers_Reject extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id'
    );

    /**
     * Reject answers.
     *
     * @author truongnn
     * @param array $data Input array.
     * @return bool Success or otherwise.
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Answer::reject($data);
            return $this->result(\Model_Answer::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
