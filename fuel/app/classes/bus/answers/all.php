<?php

namespace Bus;

/**
 * Get all Answer (using for mobile)
 *
 * @package Bus
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Answers_All extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'question_id'   => array(1, 128),
        'user_number'   => array(1, 11),
        'login_user_id' => array(1, 11),
        'status'        => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'question_id',
        'user_number',
        'login_user_id',
        'status'
    );

    /**
     * Call function get_all() from model Answer
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Answer::get_all($data);
            return $this->result(\Model_Answer::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
