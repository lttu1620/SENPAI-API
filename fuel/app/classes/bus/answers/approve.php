<?php

namespace Bus;

/**
 * Approve list Answers.
 *
 * @package Bus
 * @version 1.0
 * @author truongnn
 * @copyright Oceanize INC
 */
class Answers_Approve extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'id'
    );

    /**
     * Approve answers.
     *
     * @author truongnn
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Answer::approve($data);
            return $this->result(\Model_Answer::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
