<?php

namespace Bus;

/**
 * Get list Answer (using for admin page)
 *
 * @package Bus
 * @created 2015-03-13
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Answers_List extends BusAbstract
{
    /** @var array $_length Length of fields */
    protected $_length = array(
        'question_id'   => array(1, 128),
        'user_number'   => array(1, 11),
        'login_user_id' => array(1, 11),
        'disable'       => 1,
        'status'        => 1
    );

    /** @var array $_number_format field number */
    protected $_number_format = array(
        'page',
        'limit',
        'question_id',
        'user_number',
        'login_user_id',
        'disable',
        'status'
    );

    /**
     * Call function get_list() from model Answer
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Answer::get_list($data);
            return $this->result(\Model_Answer::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
