<?php

namespace Bus;

/**
 * Update field nice_count for Answer
 *
 * @package Bus
 * @created 2015-03-18
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Answers_UpdateCounter extends BusAbstract
{
    /** @var array $_required field require */
    protected $_required = array(
        'answer_id'
    );

    /**
     * Call function update_counter() from model Answer
     *
     * @author Le Tuan Tu
     * @param array $data Input data
     * @return bool Success or otherwise
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Answer::update_counter($data);
            return $this->result(\Model_Answer::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }
}
