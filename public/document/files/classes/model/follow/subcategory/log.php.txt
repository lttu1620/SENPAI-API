<?php

/**
 * Any query for model follow subcategory log.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Follow_Subcategory_Log extends Model_Abstract {

    protected static $_properties = array(
        'id',
        'user_id',
        'subcategory_id',
        'unfollow',
        'created',
    );
    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events' => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events' => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );
    protected static $_table_name = 'follow_subcategory_logs';

    /**
     * Add info for follow subcategory log.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @throws Exception If the provided is not of type array.
     * @return bool Returns the boolean.
     */
    public static function add($param) {
        try {
            $options['where'][] = array(
                'subcategory_id' => $param['subcategory_id'],
                'user_id' => $param['user_id']
            );
            $log = self::find('first', $options);
            if (!$log) {
                $log = new self;
                $log->set('subcategory_id', $param['subcategory_id']);
                $log->set('user_id', $param['user_id']);
                $log->set('unfollow', $param['unfollow']);
                if ($log->create()) {
                    return true;
                }
            } else {
                if ($log->_data['unfollow'] != $param['unfollow']) {
                    $log->set('unfollow', $param['unfollow']);
                    if ($log->update()) {
                        return true;
                    }
                } else {
                    self::errorDuplicate('follow_subcategory_log_id');
                    return false;
                }
            }

            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get list follow subcategory log.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array include total and data.
     */
    public static function get_list($param) {
        $query = DB::select(
                        array('subcategories.name', 'subcategory_name'),
                        array('users.name', 'user_name'), 
                        array('users.nickname', 'user_nickname'), 
                        array('user_profiles.email', 'email'), 
                        self::$_table_name . '.*'
                )
                ->from(self::$_table_name)
                ->join('subcategories', 'LEFT')
                ->on(self::$_table_name . '.subcategory_id', '=', 'subcategories.id')
                ->join('users', 'LEFT')
                ->on(self::$_table_name . '.user_id', '=', 'users.id')
                ->join('user_profiles', 'LEFT')
                ->on(self::$_table_name . '.user_id', '=', 'user_profiles.user_id');

        if (!empty($param['unfollow'])) {
            $query->where('unfollow', '=', $param['unfollow']);
        }
        if (!empty($param['subcategory_id'])) {
            $query->where('subcategory_id', '=', $param['subcategory_id']);
        }
        if (!empty($param['name'])) {
            $query->where('users.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['nickname'])) {
            $query->where('users.nickname', 'LIKE', "%{$param['nickname']}%");
        }
        if (!empty($param['email'])) {
            $query->where('user_profiles.email', 'LIKE', "%{$param['email']}%");
        }
        if (!empty($param['date_from'])) {
            $query->where(self::$_table_name . '.created', '>=', self::date_from_val($param['date_from']));
        }
        if (!empty($param['date_to'])) {
            $query->where(self::$_table_name . '.created', '<=', self::date_to_val($param['date_to']));
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            if ($sortExplode[0] == 'created') {
                $sortExplode[0] = self::$_table_name . '.created';
            }
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

}

