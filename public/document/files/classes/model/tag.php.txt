<?php

/**
 * Any query in Model Tag.
 *
 * @package Model
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Model_Tag extends Model_Abstract
{
    protected static $_properties = array(
        'id',
        'name',
        'color',
        'feed_count',
        'disable',
        'created',
        'updated'
    );

    protected static $_observers = array(
        'Orm\Observer_CreatedAt' => array(
            'events'          => array('before_insert'),
            'mysql_timestamp' => false,
        ),
        'Orm\Observer_UpdatedAt' => array(
            'events'          => array('before_update'),
            'mysql_timestamp' => false,
        ),
    );

    protected static $_table_name = 'tags';

    /**
     * Add and update info of tag.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool|int Returns the boolean or the integer.
     */
    public static function add_update($param)
    {
        $id = !empty($param['id']) ? $param['id'] : 0;
        $tag = new self;
        if (!empty($id)) {
            $tag = self::find($id);
            if (empty($tag)) {
                static::errorNotExist('tag_id', $id);

                return false;
            }
        }

        if (!empty($param['name'])) {
            $tag->set('name', $param['name']);
        }
        if (!empty($param['color'])) {
            $tag->set('color', $param['color']);
        }
        if (isset($param['feed_count']) && $param['feed_count'] != '') {
            $tag->set('feed_count', $param['feed_count']);
        }

        if ($tag->save()) {
            if (empty($tag->id)) {
                $tag->id = self::cached_object($tag)->_original['id'];
            }

            return !empty($tag->id) ? $tag->id : 0;
        }

        return false;
    }

    /**
     * Get detail tag.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_detail($param)
    {
        $query = DB::select()
            ->from(self::$_table_name)
            ->where(self::$_table_name . '.id', '=', $param['id']);
        $data = $query->execute()->as_array();
        if (empty($data)) {
            static::errorNotExist('id', $id);
        }
        return $data ? $data[0] : array();
    }

    /**
     * Disable/enable tags.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function disable($param)
    {
        $ids = explode(',', $param['id']);
        foreach ($ids as $id) {
            $tag = self::find($id);
            if ($tag) {
                $tag->set('disable', $param['disable']);
                if (!$tag->update()) {
                    return false;
                }
            } else {
                static::errorNotExist('tag_id', $id);
                return false;
            }
        }

        return true;
    }

    /**
     * Get all tags.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns the array.
     */
    public static function get_all($param)
    {
        $query = DB::select(
                'id',
                'name',
                'color'
            )
            ->from(self::$_table_name)
            ->where('disable', '0');

        $data = $query->execute()->as_array();

        return $data;
    }

    /**
     * Get list tags.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return array Returns array(total, data).
     */
    public static function get_list($param)
    {
        $query = DB::select()
            ->from(self::$_table_name);
        if (!empty($param['name'])) {
            $query->where(self::$_table_name . '.name', 'LIKE', "%{$param['name']}%");
        }
        if (!empty($param['color'])) {
            $query->where(self::$_table_name . '.color', 'LIKE', "%{$param['color']}%");
        }
        if (isset($param['disable']) && $param['disable'] != '') {
            $query->where(self::$_table_name . '.disable', '=', $param['disable']);
        }
        if (!empty($param['sort'])) {
            $sortExplode = explode('-', $param['sort']);
            $query->order_by($sortExplode[0], $sortExplode[1]);
        } else {
            $query->order_by(self::$_table_name . '.created', 'DESC');
        }
        if (!empty($param['page']) && !empty($param['limit'])) {
            $offset = ($param['page'] - 1) * $param['limit'];
            $query->limit($param['limit'])->offset($offset);
        }
        $data = $query->execute()->as_array();
        $total = !empty($data) ? DB::count_last_query() : 0;

        return array($total, $data);
    }

    /**
     * Update any counter for tag.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     *
     */
    public static function update_counter($param)
    {
        $idArray = explode(',', $param['id']);
        foreach ($idArray as $id) {
            $sql = "
                UPDATE tags
                SET    feed_count = (
                           SELECT   COUNT(*)
                           FROM     news_feed_tags
                           WHERE    tag_id = {$id}
                               AND  disable = 0
                               AND  EXISTS ( 
                                    SELECT  news_feeds.id 
                                    FROM    news_feeds LEFT JOIN news_sites ON news_feeds.news_site_id = news_sites.id 
                                    WHERE   news_feed_tags.news_feed_id = news_feeds.id
                                            AND (news_sites.disable = 0 OR IFNULL(news_feeds.news_site_id, 0) = 0)
                                            AND news_feeds.disable = 0
                                )
                       )
                WHERE  id = {$id}
                   AND disable = 0
            ";
            DB::query($sql)->execute();
        }
        return true;
    }
     /**
     * Update counter by site.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     */
    public static function update_counter_by_site($siteId = 0)
    {
        $rows = DB::select(DB::expr('DISTINCT
                news_feed_tags.tag_id, 
                news_feeds.news_site_id'
            ))
            ->from('news_feed_tags')
            ->join('news_feeds')
            ->on('news_feed_tags.news_feed_id', '=', 'news_feeds.id')
            ->where('news_feeds.news_site_id', $siteId)->execute()->as_array();          
        if (!empty($rows)) {
            foreach ($rows as $row) {
                self::update_counter(array('id' => $row['tag_id']));
            }
        }
        return false;
    }
     /**
     * Update counter by news_feed.
     *
     * @author Le Tuan Tu
     * @param array $param Input data.
     * @return bool Returns the boolean.
     *
     */
    public static function update_counter_by_news_feed($newsFeedId = 0)
    {
        $rows = DB::select(DB::expr('DISTINCT
                news_feed_tags.tag_id, 
                news_feeds.news_site_id'
            ))
            ->from('news_feed_tags')  
            ->join('news_feeds')
            ->on('news_feed_tags.news_feed_id', '=', 'news_feeds.id')
            ->where('news_feeds.id', $newsFeedId)->execute()->as_array();         
        if (!empty($rows)) {
            foreach ($rows as $row) {
                self::update_counter(array('id' => $row['tag_id']));                
            }
        }
        return false;
    }
}

