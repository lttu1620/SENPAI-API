<?php

namespace Bus;

/**
 * get list Departments
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Departments_All extends BusAbstract
{
    protected $_number_format = array(
        'university_id'
    );

    /**
     * call function get_all() from model department
     *
     * @created 2014-11-26
     * @updated 2014-11-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $options['where'][] = array(
                'disable' => 0
            );
            if (!empty($param['university_id'])) {
                $options['where'][] = array(
                    'university_id' => $param['university_id']
                );
            }
            $this->_response = \Model_Department::find('all', $options);
            return $this->result(\Model_Department::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

