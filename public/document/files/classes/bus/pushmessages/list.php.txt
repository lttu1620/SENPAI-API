<?php

namespace Bus;

/**
 * get list push messages
 *
 * @package Bus
 * @created 2014-12-02
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class PushMessages_List extends BusAbstract
{

    //check length
    protected $_length = array(
        'company_id' => array(1, 11),
        'message' => array(1, 40),
        'android_icon' => array(1, 10),
        'ios_sound' => 1,
        'send_reservation_date' => array(0, 11),
        'rich_url' => array(0, 255),
        'is_sent' => 1,
        'sent_date' => array(1, 11),
        'disable' => 1,
    );
    //check number
    protected $_number_format = array(
        'company_id',
        'ios_sound',
        'send_reservation_date',
        'is_sent',
        'sent_date',
        'disable'
    );
    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    /**
     * call function get_list() from model Push Messages
     *
     * @created 2014-12-02
     * @updated 2014-12-02
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Push_Message::get_list($data);
            return $this->result(\Model_Push_Message::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

