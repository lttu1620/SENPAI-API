<?php

namespace Bus;

/**
 * get news detail tada copy relation
 *
 * @package Bus
 * @created 2015-01-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TadaCopyRelations_NewsDetail extends BusAbstract
{
    protected $_required = array(
        'feed_id',
        'tadacopy_app_id'
    );

    protected $_number_format = array(
        'feed_id'
    );

    /**
     * get news detail info by news feed id
     *
     * @created 2015-01-12
     * @updated 2015-01-12
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $data['apply_app_id'] = $data['tadacopy_app_id'];
            $this->_response      = \Model_News_Feed::get_news_detail_for_tada_copy($data);
            return $this->result(\Model_News_Feed::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

