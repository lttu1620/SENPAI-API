<?php

namespace Bus;

/**
 * apply like from tada copy relation
 *
 * @package Bus
 * @created 2015-02-11
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TadaCopyRelations_NewsCommentLike extends BusAbstract
{
    protected $_required = array(
        'news_comment_id',
        'tadacopy_app_id'
    );

    protected $_number_format = array(
        'news_comment_id'
    );

    /**
     * get news detail info by news feed id
     *
     * @created 2015-01-12
     * @updated 2015-01-12
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_News_Comment_Like::get_news_comment_like_for_tada_copy($data);
            return $this->result(\Model_News_Comment_Like::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

