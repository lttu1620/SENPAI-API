<?php

namespace Bus;

/**
 * get company user list tada copy relation
 *
 * @package Bus
 * @created 2015-01-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class TadaCopyRelations_CompanyUserList extends BusAbstract
{

	protected $_required = array(
		'recruiter_id',
		'tadacopy_app_id',
	);

	protected $_number_format = array(
		'recruiter_id'
	);

	/**
	 * get company user list info by company id
	 *
	 * @created 2015-01-12
	 * @updated 2015-01-12
	 * @access public
	 * @author Le Tuan Tu
	 * @param $data
	 * @return bool
	 * @example
	 */
	public function operateDB($data)
	{
		try {
			$data['apply_app_id'] = $data['tadacopy_app_id'];
			$this->_response = \Model_User_Recruiter::get_company_user_list_for_tada_copy($data);
			return $this->result(\Model_User_Recruiter::error());
		} catch (\Exception $e) {
			$this->_exception = $e;
		}
		return false;
	}

}

