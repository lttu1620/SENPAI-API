<?php

namespace Bus;

/**
 * <CompanyViewLogs_List - API to get list of CompanyViewLogs>
 *
 * @package Bus
 * @created 2014-11-25
 * @updated 2014-12-12
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class CompanyViewLogs_List extends BusAbstract
{
    protected $_email_format = array(
        'email'
    );

    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    protected $_number_format = array(
        'page',
        'limit',
        'user_id',
        'company_id',
        'share_type'
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Company_View_Log::get_list($data);
            return $this->result(\Model_Company_View_Log::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

