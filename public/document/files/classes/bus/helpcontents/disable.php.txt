<?php

namespace Bus;

/**
 * Enable/Disable help content
 *
 * @package Bus
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class HelpContents_Disable extends BusAbstract
{

    protected $_required = array(
        'id',
        'disable'
    );
    //check length
    protected $_length = array(
        'disable' => 1
    );
    //check number
    protected $_number_format = array(
        'disable'
    );

    /**
     * call function disable() from model Help Content
     *
     * @created 2014-12-04
     * @updated 2014-12-04
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Help_Content::disable($data);
            return $this->result(\Model_Help_Content::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

