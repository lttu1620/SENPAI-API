<?php

namespace Bus;

/**
 * get report news comment likes
 *
 * @package Bus
 * @created 2014-12-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Reports_NewsCommentLikes extends BusAbstract
{

    //check length
    protected $_length = array(
        'disable' => 1,
    );
    //check number
    protected $_number_format = array(
        'disable',
    );
    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    /**
     * get report news comment likes
     *
     * @created 2014-12-26
     * @updated 2014-12-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Comment_Like::get_like_report_by_date($data);
            return $this->result(\Model_News_Comment_Like::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

