<?php

namespace Bus;

/**
 * add info for follow sub category
 *
 * @package Bus
 * @created 2014-12-09
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class FollowSubcategories_Add extends BusAbstract
{

    protected $_required = array(
        'subcategory_id',
        'user_id'
    );
    //check length
    protected $_length = array(
        'subcategory_id' => array(1, 11),
        'user_id' => array(1, 11)
    );
    //check number
    protected $_number_format = array(
        'subcategory_id',
        'user_id'
    );

    /**
     * call function add() from model Follow Subcategories
     *
     * @created 2014-12-09
     * @updated 2014-12-09
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Follow_Subcategory::add($data);
            return $this->result(\Model_Follow_Subcategory::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

