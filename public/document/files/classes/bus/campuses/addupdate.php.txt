<?php

namespace Bus;

/**
 * add or update info for campus
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Campuses_AddUpdate extends BusAbstract
{
    protected $_number_format = array(
        'id',
        'university_id',
        'prefecture_id'
    );

    /**
     * call function add_update() from model Campus
     *
     * @created 2014-11-26
     * @updated 2014-11-26
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Campus::add_update($data);
            return $this->result(\Model_Campus::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

