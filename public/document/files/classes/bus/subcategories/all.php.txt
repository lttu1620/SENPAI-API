<?php

namespace Bus;

/**
 * get list subcategories
 *
 * @package Bus
 * @created 2014-12-10
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Subcategories_All extends BusAbstract
{

    /**
     * call function get_all() from model Subcategory
     *
     * @created 2014-12-10
     * @updated 2014-12-10
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $conditions['where'][] = array(
                'disable' => 0
            );
            $this->_response = \Model_Subcategory::find('all', $conditions);
            return $this->result(\Model_Subcategory::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

