<?php

namespace Bus;

/**
 * add info for push message open logs
 *
 * @package Bus
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class PushMessageOpenLogs_Add extends BusAbstract
{

    protected $_required = array(
        'user_id',
        'message_id'
    );
    //check length
    protected $_length = array(
        'user_id' => array(1, 11),
        'message_id' => array(1, 11),
    );
    //check number
    protected $_number_format = array(
        'user_id',
        'message_id'
    );

    /**
     * call function add() from model push message open Logs
     *
     * @created 2014-12-04
     * @updated 2014-12-04
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Push_Message_Open_Log::add($data);
            return $this->result(\Model_Push_Message_Open_Log::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

