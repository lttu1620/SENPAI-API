<?php

namespace Bus;

/**
 * <Mobile_NewsFeeds_List - API to get list of NewsFeeds on mobile>
 *
 * @package Bus
 * @created 2014-11-25
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Mobile_NewsFeeds_List extends BusAbstract
{

    protected $_required = array(
        'user_id',
        'order',
    );
    //check length
    protected $_length = array(
        'user_id' => array(1, 11),
        'order' => 1,
    );
    //check number
    protected $_number_format = array(
        'user_id',
        'order',
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Feed::mobile_get_list($data);
            return $this->result(\Model_News_Feed::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

