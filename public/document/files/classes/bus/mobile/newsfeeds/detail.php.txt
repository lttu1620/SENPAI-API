<?php

namespace Bus;

/**
 * <Mobile_NewsFeeds_Detail - API to get detail of NewsFeeds on mobile>
 *
 * @package Bus
 * @created 2014-11-26
 * @version 1.0
 * @author <diennvt>
 * @copyright Oceanize INC
 */
class Mobile_NewsFeeds_Detail extends BusAbstract
{

    protected $_required = array(
        'user_id',
        'news_feed_id'
    );

    //check length
    protected $_length = array(
        'user_id' => array(1, 11),
        'news_feed_id' => array(1, 11),
    );
    
    //check number
    protected $_number_format = array(
        'news_feed_id',
        'user_id',
    );
    
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_News_Feed::mobile_get_detail($data);
            return $this->result(\Model_News_Feed::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

