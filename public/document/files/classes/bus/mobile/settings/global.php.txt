<?php

namespace Bus;

/**
 * <Mobile_Usersettings_List - API to get list of Mobile Usersettings on mobile>
 *
 * @package Bus
 * @created 2014-12-19
 * @version 1.0
 * @author <tuancd>
 * @copyright Oceanize INC
 */
class Mobile_Settings_Global extends BusAbstract {
   
    public function operateDB($data) {
        try {
            $response = \Model_Setting::get_all(array('type' => 'global'));            
            $result = array();
            if (!empty($response)) {
                foreach ($response as $item) {                
                    if (strpos($item->get('name'), 'mobile', 0) !== false) {                   
                        $result[] = array(
                            'name' => $item->get('name'), 
                            'value' => $item->get('value')
                        );                        
                    }
                }
            }
            $this->_response = $result;
            return $this->result(\Model_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

