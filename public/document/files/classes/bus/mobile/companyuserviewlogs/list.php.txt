<?php

namespace Bus;

/**
 * <Mobile_CompanyUserViewLogs_List - API to get list of CompanyUserViewLogs>
 *
 * @package Bus
 * @created 2014-12-12
 * @version 1.0
 * @author <truongnn>
 * @copyright Oceanize INC
 */
class Mobile_CompanyUserViewLogs_List extends BusAbstract
{

    //check length
    protected $_length = array(
        'user_id' => array(1, 11),
        'name' => array(0, 40),
        'nickname' => array(0, 40),
        'company_id' => array(1, 11),
        'share_type' => array(1, 2),
    );
    
    //check number
    protected $_number_format = array(
        'user_id',
        'company_id',
        'share_type',
        'page',
        'limit'
    );
    
    //check email
    protected $_email_format = array(
        'email',
    );
    
    //check date
    protected $_date_format = array(
        'date_from' => 'Y-m-d',
        'date_to' => 'Y-m-d'
    );

    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_Company_User_View_Log::get_list($data);
            return $this->result(\Model_Company_User_View_Log::error());
        }
        catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

