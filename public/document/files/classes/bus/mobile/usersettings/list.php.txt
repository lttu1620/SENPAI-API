<?php

namespace Bus;

/**
 * <Mobile_Usersettings_List - API to get list of Mobile Usersettings on mobile>
 *
 * @package Bus
 * @created 2014-12-19
 * @version 1.0
 * @author <tuancd>
 * @copyright Oceanize INC
 */
class Mobile_Usersettings_List extends BusAbstract {

    protected $_required = array(
        'user_id'
    );
    protected $_length = array(
        'user_id' => array(1, 11),
    );
    protected $_number_format = array(
        'user_id',
    );

    public function operateDB($data) {
        try {
            $this->_response = \Model_User_Setting::mobile_get_list($data);
            return $this->result(\Model_User_Setting::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

