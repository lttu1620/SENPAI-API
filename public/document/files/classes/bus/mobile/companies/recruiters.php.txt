<?php

namespace Bus;

/**
 * get company's recruiter list for mobile
 *
 * @package Bus
 * @created 2014-12-18
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Mobile_Companies_Recruiters extends BusAbstract
{

    protected $_required = array(
        'company_id'
    );
    //check length
    protected $_length = array(
        'company_id' => array(1, 11),
    );
    //check number
    protected $_number_format = array(
        'company_id',
    );

    /**
     * get company's recruiter list by company_id
     *
     * @created 2014-12-18
     * @updated 2014-12-18
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try
        {
            $this->_response = \Model_User_Recruiter::get_recruiter_for_mobile($data);
            return $this->result(\Model_User_Recruiter::error());
        } catch (\Exception $e)
        {
            $this->_exception = $e;
        }
        return false;
    }

}

