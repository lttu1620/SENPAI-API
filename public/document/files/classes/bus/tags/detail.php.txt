<?php

namespace Bus;

/**
 * get detail tag
 *
 * @package Bus
 * @created 2015-01-16
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Tags_Detail extends BusAbstract
{
    // check require
    protected $_required = array(
        'id'
    );

    //check length
    protected $_length = array(
        'id' => array(1, 11),
    );

    //check number
    protected $_number_format = array(
        'id'
    );

    /**
     * get detail tag by id
     *
     * @created 2015-01-16
     * @updated 2015-01-16
     * @access public
     * @author Le Tuan Tu
     * @param $data
     * @return bool
     * @example
     */
    public function operateDB($data)
    {
        try {
            $this->_response = \Model_Tag::get_detail($data);
            return $this->result(\Model_Tag::error());
        } catch (\Exception $e) {
            $this->_exception = $e;
        }
        return false;
    }

}

