<?php

/**
 * class Str - Support functions for String
 *
 * @package Lib
 * @created 2014-11-25
 * @version 1.0
 * @author thailh
 * @copyright Oceanize INC
 */
namespace Lib;

use Fuel\Core\Security;

class Str {

    /**
     * Create a random string
     *  
     * @author thailh
     * @param int $length Length of random string
     * @param string $chars String for random
     * @return string Random string
     */
    public static function random($length = 5, $chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ') {
        $chars .= $chars . time();
        $cnt = 0;
        $result = '';
        do {
            $result .= substr($chars, rand(0, strlen($chars) - 1), 1);
            $cnt++;
        } while ($cnt < $length);
        return $result;
    }

    /**
     * Generate token for api   
     *  
     * @author thailh
     * @return string Token string
     */
    public static function generate_token_for_api() {
        return Security::generate_token();
    }

    /**
     * Generate token    
     *  
     * @author thailh
     * @return string Token string
     */
    public static function generate_token() {
        return Security::generate_token();       
    }

    /**
     * Generate token if forgetting password for mobile   
     *  
     * @author thailh
     * @return string Token string
     */
    public static function generate_token_forget_password_for_mobile() {
        return Security::generate_token(); 
    }

    /**
     * Generate app id
     *  
     * @author thailh
     * @param int $userId Id of user
     * @return string App id
     */
    public static function generate_app_id($userId) {
        return $userId . static::random(2, 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz');
    }

    /**
     * Truncate a string   
     *  
     * @author thailh
     * @param string $string String for truncating
     * @param int $limit Limit character for string after truncating
     * @param string $continuation Continuation
     * @param bool $is_html If true will return html format
     * @return string String after truncating
     */
    public static function truncate($string, $limit, $continuation = '...', $is_html = false) {
        return \Fuel\Core\Str::truncate($string, $limit, $continuation, $is_html);
    }

    /**
     * Method startsWith - check if start with   
     *  
     * @author thailh
     * @param string $haystack A string to search
     * @param string $needle If needle is not a string, it is converted to an integer and applied as the ordinal value of a character.
     * @return bool Return true if successful ortherwise return false
     */
    public static function startsWith($haystack, $needle) {
        return $needle === "" || strrpos($haystack, $needle, -strlen($haystack)) !== FALSE;
    }

    /**
     * Method endsWith - check if end with   
     *  
     * @author thailh
     * @param string $haystack A string to search
     * @param string $needle If needle is not a string, it is converted to an integer and applied as the ordinal value of a character.
     * @return bool Return true if successful ortherwise return false
     */
    public static function endsWith($haystack, $needle) {
        return $needle === "" || strpos($haystack, $needle, strlen($haystack) - strlen($needle)) !== FALSE;
    }

}

