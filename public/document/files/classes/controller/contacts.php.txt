<?php

/**
 * Controller for actions on Contacts
 *
 * @package Controller
 * @created 2014-12-12
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_Contacts extends \Controller_App
{
    /**
     * Get list Contact
     *  
     * @return boolean
     */
    public function action_list() {
        return \Bus\Contacts_List::getInstance()->execute();
    }

    /**
     * Update or add new informaion for Contact  
     *  
     * @return boolean
     */
    public function action_addUpdate() {
        return \Bus\Contacts_AddUpdate::getInstance()->execute();
    }

    /**
     * Get detail of Contact  
     *  
     * @return boolean
     */
    public function action_detail() {
        return \Bus\Contacts_Detail::getInstance()->execute();
    }

}
