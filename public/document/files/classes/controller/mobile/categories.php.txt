<?php

/**
 * Controller_Mobile_Categories - Controller for actions on Categories on mobile
 *
 * @package Controller
 * @created 2014-11-25
 * @version 1.0
 * @author diennvt
 * @copyright Oceanize INC
 */
class Controller_Mobile_Categories extends \Controller_App
{

    /**
     * Action post_list   
     *  
     * @author diennvt 
     * @return void
     */
    public function post_list()
    {
        return \Bus\Mobile_Categories_List::getInstance()->execute();
    }
    
    /**
     * Action get_list   
     *  
     * @author diennvt 
     * @return void
     */
    public function get_list()
    {
        return $this->post_list();
    }
}

