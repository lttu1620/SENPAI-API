<?php

/**
 * Controller for actions on News Comments
 *
 * @package Controller
 * @created 2014-11-26
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_NewsComments extends \Controller_App
{
    /**
     *  Get Detail of NewsComment
     * 
     * @return boolean 
     */
    public function action_detail() {
        return \Bus\NewsComments_Detail::getInstance()->execute();
    }

    /**
     *  Get List of NewsComment
     * 
     * @return boolean 
     */
    public function action_list() {
        return \Bus\NewsComments_List::getInstance()->execute();
    }

    /**
     *  Update disable field for NewsComment
     * 
     * @return boolean 
     */
    public function action_disable() {
        return \Bus\NewsComments_Disable::getInstance()->execute();
    }

    /**
     *  Add new  NewsComment
     * 
     * @return boolean 
     */
    public function action_add() {
        return \Bus\NewsComments_Add::getInstance()->execute();
    }

    /**
     *  Update NewsComment
     * 
     * @return boolean 
     */
    public function action_update() {
        return \Bus\NewsComments_Update::getInstance()->execute();
    }

    /**
     *  Update counter of NewsComment
     * 
     * @return boolean 
     */
    public function post_updateCounter()
    {
        return \Bus\NewsComments_UpdateCounter::getInstance()->execute();
    }
}
