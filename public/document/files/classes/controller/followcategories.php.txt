<?php

/**
 * Controller for actions on follow category
 *
 * @package Controller
 * @created 2014-12-04
 * @version 1.0
 * @author Le Tuan Tu
 * @copyright Oceanize INC
 */
class Controller_FollowCategories extends \Controller_App
{
    /**
     * Get Detail of FollowCategory by action GET
     *  
     * @return boolean
     */
    public function get_detail() {
        return $this->post_detail();
    }
    
    /**
     * Get Detail of FollowCategory by action POST
     *  
     * @return boolean
     */
    public function post_detail() {
        return \Bus\FollowCategories_Detail::getInstance()->execute();
    }

    /**
     * Get list of FollowCategory by action GET
     *  
     * @return boolean
     */
    public function get_list() {
        return $this->post_list();
    }

    /**
     * Get list of FollowCategory by action POST
     *  
     * @return boolean
     */
    public function post_list() {
        return \Bus\FollowCategories_List::getInstance()->execute();
    }

    /**
     * Update disable field for FollowCategory s
     *  
     * @return boolean
     */
    public function action_disable() {
        return \Bus\FollowCategories_Disable::getInstance()->execute();
    }

    /**
     * Add new FollowCategory 
     *  
     * @return boolean
     */
    public function action_add() {
        return \Bus\FollowCategories_Add::getInstance()->execute();
    }

}
